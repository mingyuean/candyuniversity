# candyuniversity

### 服务划分

其他管理（人事、财务、招生、图书、活动流程）

> 端口：

#### renrenfast 人人开源

- 端口：8080
- 登录、菜单管理

#### gateway 网关

- 端口：88

#### teaching 教学管理

（主要是老师做的操作）

- 端口：1001
- 数据库名：univ_teaching
- 表头：tcg_
- 班级、课程、教师、教室、分课

#### nomenclature 命名法

（专业术语）

- 端口：2001
- 数据库名：univ_nomenclature
- 表头：tee_
- 分类、缩写、词汇、翻译
- classification、abbreviation、vocabulary、translate

#### usercenter 用户中心

（登录注册）

- 端口：2001

#### rbac 权限管理

- 端口：3001

#### student 学生管理

（主要是学生做的操作）

- 端口：4001

#### community 社区论坛

（主要是一个讨论区）

### 人人开源

> renren-fast
> renren-fast-vue
> renren-gen

```properties
#代码生成器，配置信息
mainPath=com.mingyuean.university
#包名
package=com.mingyuean.university
moduleName=nomenclature
#作者
author=MingYueAn
#Email
email=MingYueAn@email.com
#表前缀(类名不会包含表前缀)
tablePrefix=tee_
```

### 项目构建

共同：web、openfeign

包名：com.mingyuean.university.xxx

模块名：university-xxx

服务名：university-xxx

应用名：university-xxx

数据库名：univ_xxx

![](https://s2.loli.net/2022/05/09/v7eCUWZst1Hm6IR.png)

![](https://s2.loli.net/2022/05/09/OzIU8dZyvqg7Yfc.png)

### 技术搭配方案

#### SpringCloud Alibaba＋SpringCloud

> SpringCloud Alibaba - Nacos 注册中心（服务发现／注册）
> SpringCloud Alibaba - Nacos 配置中心（动态配置）
> SpringCloud - loadbalancer 负载均衡
> SpringCloud - OpenFeign 声明式HTTP客户端（调用远程服务）
> SpringCloud Alibaba - Sentinel 服务容错（降级、限流、熔断）
> SpringCloud - Gateway API 网关（webflux编程模式）
> SpringCloud - Sleuth 调用链监控
> SpringCloud Alibaba - Seata 分布式事物解决方案

### nacos-server-1.4.3（认证中心、配置中心）

- 服务发现
- 服务健康监测
- 动态配置服务
- 动态DNS服务
- 服务及其元数据管理

一致性协议

CAP 
C一致性
A可用性P分区容错性

```java
@EnableDiscoveryClient//服务注册
@MapperScan("com.mingyuean.university.teaching.dao")//mybaits plus
@EnableFeignClients(basePackages = "com.mingyuean.university.teaching.feign")//远程调用
@RefreshScope//动态加载配置
```

配置中心的配置集优先使用

- 命名空间：配置隔离，默认命名空间public
  - 开发、测试、生产的配置不一样，可创建多个命名空间
  - 每个微服务可分别创建命名空间，只加载自己命名空间下的配置
- 配置集：所有配置的集合（一个配置文件就相当于一个配置集）
- 配置集id： Data Id类似配置文件名
- 配置分组：默认所有配置集都属于 DEFAULT_GROUP 配置组

当前项目使用配置分组区分环境，使用命名空间区分微服务

```
# nacos命名空间选择
spring.cloud.nacos.config.namespace=c8accf98-c5b7-498c-a8c4-e0a9668d57b2
# 配置分组选择（加载默认配置分组选择）
spring.cloud.nacos.config.group=test
```

可加载多个配置集：    bootstrap.properties

```properties
# 应用名
spring.application.name=university-teaching
# nacos配置中心地址
spring.cloud.nacos.config.server-addr=127.0.0.1:8848
# nacos命名空间选择
spring.cloud.nacos.config.namespace=c8accf98-c5b7-498c-a8c4-e0a9668d57b2
# 配置分组选择（加载默认配置分组选择）
spring.cloud.nacos.config.group=test

spring.cloud.nacos.config.extension-configs[0].data-id=other.yml
spring.cloud.nacos.config.extension-configs[0].group=dev
spring.cloud.nacos.config.extension-configs[0].refresh=true

spring.cloud.nacos.config.extension-configs[1].data-id=mybaits.yml
spring.cloud.nacos.config.extension-configs[1].group=dev
spring.cloud.nacos.config.extension-configs[1].refresh=true

spring.cloud.nacos.config.extension-configs[2].data-id=datasource.yml
spring.cloud.nacos.config.extension-configs[2].group=dev
spring.cloud.nacos.config.extension-configs[2].refresh=true
```

原配置：   application.yml

```yml
# 设置端口
server:
  port: 5001
spring:
  # MySQL配置
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/univ_teaching?useSSH=false&useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC
    username: root
    password:
  # Redis配置
  redis:
    database: 0 #数据库名
    host: localhost #服务器地址
    port: 6379 #连接端口号
    password: 123456
  # nacos注册中心地址
  cloud:
    nacos:
      server-addr: 127.0.0.1:8848
  # 服务名
  application:
    name: university-teaching
# 日志配置
logging:
  pattern:
    console: "%d{yyyy-MM-dd} [%thread] %-5level %logger{50} %L - %msg%n"
  level:
    com:
      mingyuean: debug
# mybaits plus配置
mybatis-plus:
  mapper-locations: classpath:/mapper/**/*.xml
  global-config:
    db-config:
      id-type: auto
```

### Gateway网关



### 参数校验

JSR303

参数校验依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-validation</artifactId>
    <version>2.3.7.RELEASE</version>
</dependency>
```

- Bean添加校验，开启校验功能注解（`@Valid`）

- 分组校验`@Validated({AddGroup.class})`and`groups = {AddGroup.class}`

- 自定义校验注解：自定义校验注解、自定义的校验器、两者关联


  ```java
  /**
   * 自定义校验注解
   *
   * @author MingYueAn
   */
  @Documented
  @Constraint(
          validatedBy = {ListValueConstraintValidator.class}
  )
  @Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
  @Retention(RetentionPolicy.RUNTIME)
  public @interface ListValue {
  
      String message() default "{com.mingyuean.common.validator.ListValue.message}";
  
      Class<?>[] groups() default {};
  
      Class<? extends Payload>[] payload() default {};
  
      int[] values() default {};
  }
  ```

```java
/**
 * 自定义校验器
 * @author MingYueAn
 * @Description: 自定义校验器
 * @Date: 2022/5/14 16:48
 * @Version: 1.0
 */
public class ListValueConstraintValidator implements ConstraintValidator<ListValue, Integer> {

    private final Set<Integer> set = new HashSet<>();

    /**
     * 初始化方法
     *
     * @param constraintAnnotation 自定义注解的详细信息
     */
    @Override
    public void initialize(ListValue constraintAnnotation) {
        final int[] values = constraintAnnotation.values();
        for (int v : values) {
            set.add(v);
        }
    }

    /**
     * 判断是否校验成功
     *
     * @param integer                    需要校验的字段
     * @param constraintValidatorContext 上下文环境信息
     * @return true false
     */
    @Override
    public boolean isValid(Integer integer, ConstraintValidatorContext constraintValidatorContext) {
        return set.contains(integer);
    }
}
```

统一异常处理之前

```java
public R save(@Valid @RequestBody ClassesEntity classes, BindingResult result) {
    if (result.hasErrors()) {
        //封装
        Map<String, String> map = new HashMap<>();
        //获取校验错误结果
        result.getFieldErrors().forEach((item) -> {
            //获取错误提示
            String message = item.getDefaultMessage();
            //获取错误字段名
            String field = item.getField();
            map.put(field, message);
        });
        return R.error(400, "数据不合法").put("data", map);
    } else {
        classesService.save(classes);
    }
    return R.ok();
}
```

数据校验异常处理：


```java
@ExceptionHandler(value = MethodArgumentNotValidException.class)
public R handleValidException(MethodArgumentNotValidException e) {
    log.error("数据校验异常，异常类型：{}", e.getClass());
    //获取数据校验结果
    final BindingResult bindingResult = e.getBindingResult();
    //封装
    Map<String, String> errorMap = new HashMap<>();
    //遍历FieldError
    bindingResult.getFieldErrors().forEach((item) -> {
        //获取错误提示
        String message = item.getDefaultMessage();
        //获取错误字段名
        String field = item.getField();
        //map封装
        errorMap.put(field, message);
    });
    return R.error(ResultCode.VALID_EXCEPTION.getCode(), ResultCode.VALID_EXCEPTION.getMessage()).put("data", errorMap);
}
```

### 状态码

5位数字

前两位为业务场景

10：通用

11：教师

12：权限

13：论坛

后三位为错误码

400：参数校验失败

000：未知异常