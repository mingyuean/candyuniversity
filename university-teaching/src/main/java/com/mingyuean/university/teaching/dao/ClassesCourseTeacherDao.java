package com.mingyuean.university.teaching.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mingyuean.university.teaching.entity.ClassesCourseTeacherEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 班级课程教师关联表（分配课程，为排课做准备）
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-15 09:44:25
 */
@Mapper
public interface ClassesCourseTeacherDao extends BaseMapper<ClassesCourseTeacherEntity> {

}
