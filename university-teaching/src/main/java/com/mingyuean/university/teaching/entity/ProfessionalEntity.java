package com.mingyuean.university.teaching.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mingyuean.common.validator.group.AddGroup;
import com.mingyuean.common.validator.group.UpdateGroup;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;

/**
 * 专业
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 17:48:54
 */
@Data
@TableName("tcg_professional")
public class ProfessionalEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 序号
     */
    @NotNull(message = "修改必须指定id", groups = {UpdateGroup.class})
    @Null(message = "新增不能指定id", groups = {AddGroup.class})
    @TableId
    private Long id;
    /**
     * 名字
     */
    @NotBlank(message = "请输入专业名", groups = {AddGroup.class})
    @Length(max = 10, message = "专业名不能超过个 {max} 字符", groups = {AddGroup.class, UpdateGroup.class})
    private String professionalName;

    /**
     * 编码
     */
    @NotBlank(message = "请输入专业编码", groups = {AddGroup.class})
    @Length(max = 10, message = "专业编码不能超过个 {max} 字符", groups = {AddGroup.class, UpdateGroup.class})
    private String professionalCode;
    /**
     * 专业类型
     */
    @NotBlank(message = "请输入专业类型", groups = {AddGroup.class})
//    @ListValue(values = {'0', '1', 'A', 'B'}, groups = {AddGroup.class, UpdateGroup.class})
    private String professionalType;
}
