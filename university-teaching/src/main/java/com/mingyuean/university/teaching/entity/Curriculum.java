package com.mingyuean.university.teaching.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author MingYueAn
 * @Description: 课程表
 * @Date: 2022/5/23 13:18
 * @Version: 1.0
 */
@Data
public class Curriculum implements Serializable {

    private String course;//"第一节"
    private ClassesCourseTeacherEntity one;   //星期一，第一节
    private ClassesCourseTeacherEntity two;   //星期二，第一节
    private ClassesCourseTeacherEntity three; //星期三，第一节
    private ClassesCourseTeacherEntity four;  //星期四，第一节
    private ClassesCourseTeacherEntity five;  //星期五，第一节
}
