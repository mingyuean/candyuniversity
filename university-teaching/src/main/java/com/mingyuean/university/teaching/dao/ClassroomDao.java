package com.mingyuean.university.teaching.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mingyuean.university.teaching.entity.ClassroomEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 教室
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 17:48:54
 */
@Mapper
public interface ClassroomDao extends BaseMapper<ClassroomEntity> {

}
