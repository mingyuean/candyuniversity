package com.mingyuean.university.teaching.service;

import java.util.List;

/**
 * @author MingYueAn
 * @Description: 统计
 * @Date: 2022/6/2 10:01
 * @Version: 1.0
 */
public interface StatisticsService {
    List<Integer> findCount();

    List<Integer> findCourseTypeCount();

    List<Integer> findClassroomTypeCount();

    List<Integer> findProfessionalTypeCount();
}
