package com.mingyuean.university.teaching.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.university.teaching.entity.ProfessionalEntity;

import java.util.Map;

/**
 * 专业
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 17:48:54
 */
public interface ProfessionalService extends IService<ProfessionalEntity> {

    PageUtils queryPage(Map<String, Object> params);

}

