package com.mingyuean.university.teaching.controller;

import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.R;
import com.mingyuean.common.validator.group.AddGroup;
import com.mingyuean.common.validator.group.UpdateGroup;
import com.mingyuean.university.teaching.entity.SemesterEntity;
import com.mingyuean.university.teaching.service.SemesterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author MingYueAn
 * @Description:
 * @Date: 2022/5/24 16:26
 * @Version: 1.0
 */
@RestController
@Slf4j
@RequestMapping("teaching/semester")
public class SemesterController {
    @Autowired
    private SemesterService semesterService;

    /**
     * 查询所有学期
     */
    @RequestMapping("/findAll")
    //@RequiresPermissions("teaching:classes:list")
    public R findAll() {
        final List<SemesterEntity> list = semesterService.list();
        list.forEach(item -> {
            item.setDatetime(item.getSemesterStartDate() + " 至 " + item.getSemesterEndDate());
        });
        return R.ok().put("semester", list);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("teaching:teacher:list")
    public R list(@RequestParam Map<String, Object> params) {
        params.remove("t");
        PageUtils page = semesterService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("teaching:teacher:info")
    public R info(@PathVariable("id") Long id) {
        SemesterEntity semester = semesterService.getById(id);

        return R.ok().put("semester", semester);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("teaching:teacher:save")
    public R save(@Validated({AddGroup.class}) @RequestBody SemesterEntity semester) {
        log.error("测试{}", semester);
        semesterService.save(semester);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("teaching:teacher:update")
    public R update(@Validated({UpdateGroup.class}) @RequestBody SemesterEntity semester) {
        semesterService.updateById(semester);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("teaching:teacher:delete")
    public R delete(@RequestBody Long[] ids) {
        semesterService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
