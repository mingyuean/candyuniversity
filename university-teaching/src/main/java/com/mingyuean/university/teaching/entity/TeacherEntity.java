package com.mingyuean.university.teaching.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mingyuean.common.validator.group.AddGroup;
import com.mingyuean.common.validator.group.UpdateGroup;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * 老师
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 17:48:54
 */
@Data
@TableName("tcg_teacher")
public class TeacherEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 序号
     */
    @NotNull(message = "修改必须指定id", groups = {UpdateGroup.class})
    @Null(message = "新增不能指定id", groups = {AddGroup.class})
    @TableId
    private Long id;
    /**
     * 姓名
     */
    @NotBlank(message = "请输入名字", groups = {AddGroup.class})
    @Length(max = 10, message = "名字不能超过个 {max} 字符", groups = {AddGroup.class, UpdateGroup.class})
    private String teacherName;
    /**
     * 电话
     */
    @NotBlank(message = "请输入电话", groups = {AddGroup.class})
    @Length(min = 11, max = 11, message = "手机号只能为11位", groups = {AddGroup.class, UpdateGroup.class})
    @Pattern(regexp = "^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "手机号格式有误", groups = {AddGroup.class, UpdateGroup.class})
    private String teacherTelephone;
    /**
     * <p>每周课程上限</p>
     * <p>一星期最多上25节课</p>
     * <p>注意分课判断</p>
     */
    @NotNull(message = "请输入每周课程上限", groups = {AddGroup.class})
    @Range(min = 0, max = 25, message = "每周课程上限范围为 {min} 到 {max} 之间", groups = {AddGroup.class, UpdateGroup.class})
    private Integer teacherWeekMax;

}
