package com.mingyuean.university.teaching.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.Query;
import com.mingyuean.university.teaching.dao.ClassroomDao;
import com.mingyuean.university.teaching.entity.ClassroomEntity;
import com.mingyuean.university.teaching.service.ClassroomService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("classroomService")
public class ClassroomServiceImpl extends ServiceImpl<ClassroomDao, ClassroomEntity> implements ClassroomService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<ClassroomEntity> queryWrapper = new QueryWrapper<>();
        //获取key
        String key = (String) params.get("key");
        if (StringUtils.isNotEmpty(key)) {
            //模糊查询
            queryWrapper.and((item) -> {
                item.eq("id", key);
            }).or((item) -> {
                item.like("classroom_name", key);
            });
        }

        IPage<ClassroomEntity> page = this.page(
                new Query<ClassroomEntity>().getPage(params),
                queryWrapper
        );
        return new PageUtils(page);
    }

}