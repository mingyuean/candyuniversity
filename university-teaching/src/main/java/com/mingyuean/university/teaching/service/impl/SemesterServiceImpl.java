package com.mingyuean.university.teaching.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.Query;
import com.mingyuean.university.teaching.dao.SemesterDao;
import com.mingyuean.university.teaching.entity.SemesterEntity;
import com.mingyuean.university.teaching.service.SemesterService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author MingYueAn
 * @Description:
 * @Date: 2022/5/24 16:28
 * @Version: 1.0
 */
@Service("semesterService")
public class SemesterServiceImpl extends ServiceImpl<SemesterDao, SemesterEntity> implements SemesterService {
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<SemesterEntity> queryWrapper = new QueryWrapper<>();

        IPage<SemesterEntity> page = this.page(
                new Query<SemesterEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }
}
