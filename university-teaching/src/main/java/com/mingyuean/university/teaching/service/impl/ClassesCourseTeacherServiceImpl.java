package com.mingyuean.university.teaching.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.Query;
import com.mingyuean.common.utils.R;
import com.mingyuean.university.teaching.dao.*;
import com.mingyuean.university.teaching.entity.*;
import com.mingyuean.university.teaching.service.ClassesCourseTeacherService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("classesCourseTeacherService")
public class ClassesCourseTeacherServiceImpl extends ServiceImpl<ClassesCourseTeacherDao, ClassesCourseTeacherEntity> implements ClassesCourseTeacherService {
    @Autowired
    private ClassesDao classesDao;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private TeacherDao teacherDao;
    @Autowired
    private ClassroomDao classroomDao;
    @Autowired
    private ClassesCourseTeacherDao classesCourseTeacherDao;

    /**
     * 初始化
     *
     * @param list
     * @return
     */
    @Override
    public List<ClassesCourseTeacherEntity> init(List<ClassesCourseTeacherEntity> list) {
        //id数组
        List<Long> classesIdList = new ArrayList<>();
        List<Long> courseIdList = new ArrayList<>();
        List<Long> teacherIdList = new ArrayList<>();
        List<Long> classroomIdList = new ArrayList<>();
        //添加id
        list.forEach(item -> {
            classesIdList.add(item.getClassesId());
            courseIdList.add(item.getCourseId());
            teacherIdList.add(item.getTeacherId());
            classroomIdList.add(item.getClassroomId());
        });
        //根据id生成的列表
        final List<ClassesEntity> classesEntityList = classesDao.selectBatchIds(classesIdList);
        final List<CourseEntity> courseEntityList = courseDao.selectBatchIds(courseIdList);
        final List<TeacherEntity> teacherEntityList = teacherDao.selectBatchIds(teacherIdList);
        final List<ClassroomEntity> classroomEntityList = classroomDao.selectBatchIds(classroomIdList);
        //设置实体
        for (ClassesCourseTeacherEntity classesCourseTeacherEntity : list) {
            classesEntityList.forEach(item -> {
                if (item.getId().equals(classesCourseTeacherEntity.getClassesId())) {
                    classesCourseTeacherEntity.setClassesEntity(item);
                }
            });
            courseEntityList.forEach(item -> {
                if (item.getId().equals(classesCourseTeacherEntity.getCourseId())) {
                    classesCourseTeacherEntity.setCourseEntity(item);
                }
            });
            teacherEntityList.forEach(item -> {
                if (item.getId().equals(classesCourseTeacherEntity.getTeacherId())) {
                    classesCourseTeacherEntity.setTeacherEntity(item);
                }
            });
            classroomEntityList.forEach(item -> {
                if (item.getId().equals(classesCourseTeacherEntity.getClassroomId())) {
                    classesCourseTeacherEntity.setClassroomEntity(item);
                }
            });
            //获取时间的char数组
            char[] weekNumber;
            if (StringUtils.isNotEmpty(classesCourseTeacherEntity.getWeekNumber())) {
                weekNumber = classesCourseTeacherEntity.getWeekNumber().toCharArray();
                //设置时间，数据解析
                classesCourseTeacherEntity.setTime(time(weekNumber));
            } else {
                classesCourseTeacherEntity.setTime(null);
            }
        }
        list.forEach(System.out::println);
        return list;
    }


    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<ClassesCourseTeacherEntity> queryWrapper = new QueryWrapper<>();

        QueryWrapper<ClassesEntity> classesQueryWrapper = new QueryWrapper<>();
        QueryWrapper<CourseEntity> courseQueryWrapper = new QueryWrapper<>();
        QueryWrapper<TeacherEntity> teacherQueryWrapper = new QueryWrapper<>();
        //获取key
        String classesKey = (String) params.get("classes_key");
        String courseKey = (String) params.get("course_key");
        String teacherKey = (String) params.get("teacher_key");
        //模糊查询classes
        if (StringUtils.isNotEmpty(classesKey)) {
            classesQueryWrapper.like("classes_name", classesKey);
            classesDao.selectList(classesQueryWrapper).forEach(item -> {
                queryWrapper.eq("classes_id", item.getId()).or();
            });
        }
        //模糊查询teacher
        if (StringUtils.isNotEmpty(teacherKey)) {
            teacherQueryWrapper.like("teacher_name", teacherKey);
            teacherDao.selectList(teacherQueryWrapper).forEach(item -> {
                queryWrapper.eq("teacher_id", item.getId()).or();
            });
        }
        //模糊查询course
        if (StringUtils.isNotEmpty(courseKey)) {
            courseQueryWrapper.like("course_name", courseKey);
            courseDao.selectList(courseQueryWrapper).forEach(item -> {
                queryWrapper.eq("course_id", item.getId()).or();
            });
        }

        IPage<ClassesCourseTeacherEntity> page = this.page(
                new Query<ClassesCourseTeacherEntity>().getPage(params),
                queryWrapper
        );
        page.setRecords(init(page.getRecords()));
        return new PageUtils(page);
    }

    /**
     * 保存（判断是否达到上限）
     *
     * @param classesCourseTeacher
     * @return
     */
    @Override
    public R saveByDetail(ClassesCourseTeacherEntity classesCourseTeacher) {
        //查询班级
        final ClassesEntity classesEntity = classesDao.selectById(classesCourseTeacher.getClassesId());
        //获取班级总上课数
        final Long classesCount = getClassesCount(classesCourseTeacher.getClassesId());
        //查询教师
        final TeacherEntity teacherEntity = teacherDao.selectById(classesCourseTeacher.getTeacherId());
        //获取教师已教课程数
        final Long teacherCount = getTeacherCount(classesCourseTeacher.getTeacherId());
        //教师每周课程上限 > 教师已教课程数
        if (teacherEntity.getTeacherWeekMax() > teacherCount) {
            //班级每周课程上限 > 班级总上课数
            if (classesEntity.getClassesWeekMax() > classesCount) {
                //保存
                this.save(classesCourseTeacher);
                return R.ok();
            } else {
                return R.error("该班级每天的课程已达上限，请重新分配");
            }
        } else {
            return R.error("该教师的课程已达上限，请重新分配");
        }

    }

    /**
     * 修改（判断是否达到上限）
     *
     * @param classesCourseTeacher
     */
    @Override
    public R updateByIdDetail(ClassesCourseTeacherEntity classesCourseTeacher) {
        //查询班级
        final ClassesEntity classesEntity = classesDao.selectById(classesCourseTeacher.getClassesId());
        //获取班级总上课数
        final Long classesCount = getClassesCount(classesCourseTeacher.getClassesId());
        //查询教师
        final TeacherEntity teacherEntity = teacherDao.selectById(classesCourseTeacher.getTeacherId());
        //获取教师已教课程数
        final Long teacherCount = getTeacherCount(classesCourseTeacher.getTeacherId());
        //教师每周课程上限 > 教师已交课程数
        if (teacherEntity.getTeacherWeekMax() > teacherCount) {
            //班级每周课程上限 > 班级总上课数
            if (classesEntity.getClassesWeekMax() > classesCount) {
                //修改
                this.updateById(classesCourseTeacher);
                return R.ok();
            } else {
                return R.error("该班级每天的课程已达上限，请重新分配");
            }
        } else {
            return R.error("该教师的课程已达上限，请重新分配");
        }
    }

    private Long getTeacherCount(Long teacherId) {
        QueryWrapper<ClassesCourseTeacherEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("teacher_id", teacherId);
        return classesCourseTeacherDao.selectCount(queryWrapper);
    }

    private Long getClassesCount(Long classesId) {
        QueryWrapper<ClassesCourseTeacherEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("classes_id", classesId);
        return classesCourseTeacherDao.selectCount(queryWrapper);
    }

    /**
     * 数据解析
     *
     * @param weekNumber
     * @return
     */
    private String time(char[] weekNumber) {
        String time = "";
        //数据处理
        switch (weekNumber[0]) {
            case 'A':
                time = "星期一 " + "第" + weekNumber[1] + "节课";
                break;
            case 'B':
                time = "星期二 " + "第" + weekNumber[1] + "节课";
                break;
            case 'C':
                time = "星期三 " + "第" + weekNumber[1] + "节课";
                break;
            case 'D':
                time = "星期四 " + "第" + weekNumber[1] + "节课";
                break;
            case 'E':
                time = "星期五 " + "第" + weekNumber[1] + "节课";
                break;
            default:
                time = "";
                break;
        }
        if ("".equals(time)) {
            return null;
        } else {
            return time;
        }
    }
}