package com.mingyuean.university.teaching.controller;

import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.R;
import com.mingyuean.university.teaching.entity.ClassesCourseTeacherEntity;
import com.mingyuean.university.teaching.service.ClassesCourseTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 班级课程教师关联表（分配课程，为排课做准备）
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-15 09:44:25
 */
@RestController
@RequestMapping("teaching/classescourseteacher")
public class ClassesCourseTeacherController {
    @Autowired
    private ClassesCourseTeacherService classesCourseTeacherService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("university-teaching:classescourseteacher:list")
    public R list(@RequestParam Map<String, Object> params) {
        params.remove("t");
        PageUtils page = classesCourseTeacherService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("university-teaching:classescourseteacher:info")
    public R info(@PathVariable("id") Long id) {
        ClassesCourseTeacherEntity classesCourseTeacher = classesCourseTeacherService.getById(id);

        return R.ok().put("classesCourseTeacher", classesCourseTeacher);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("university-teaching:classescourseteacher:save")
    public R save(@RequestBody ClassesCourseTeacherEntity classesCourseTeacher) {
        return classesCourseTeacherService.saveByDetail(classesCourseTeacher);
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("university-teaching:classescourseteacher:update")
    public R update(@RequestBody ClassesCourseTeacherEntity classesCourseTeacher) {
        //修改详细信息（id、name要一起修改）
        return classesCourseTeacherService.updateByIdDetail(classesCourseTeacher);
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("university-teaching:classescourseteacher:delete")
    public R delete(@RequestBody Long[] ids) {
        classesCourseTeacherService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
