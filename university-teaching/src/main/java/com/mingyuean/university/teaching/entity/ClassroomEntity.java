package com.mingyuean.university.teaching.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mingyuean.common.validator.group.AddGroup;
import com.mingyuean.common.validator.group.UpdateGroup;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;

/**
 * 教室
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 17:48:54
 */
@Data
@TableName("tcg_classroom")
public class ClassroomEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 序号
     */
    @NotNull(message = "修改必须指定id", groups = {UpdateGroup.class})
    @Null(message = "新增不能指定id", groups = {AddGroup.class})
    @TableId
    private Long id;
    /**
     * 名字
     */
    @NotBlank(message = "请输入教室名", groups = {AddGroup.class})
    @Length(max = 10, message = "教室名不能超过个 {max} 字符", groups = {AddGroup.class, UpdateGroup.class})
    private String classroomName;
    /**
     * 教室大小
     */
    @NotNull(message = "请输入教室大小", groups = {AddGroup.class})
    @Range(min = 20, max = 50, message = "教室大小范围为 {min} 到 {max} 之间", groups = {AddGroup.class, UpdateGroup.class})
    private Integer classroomSize;
    /**
     * 教室类型
     */
    @NotBlank(message = "请输入教室类型", groups = {AddGroup.class})
//    @ListValue(values = {0, 1, 2}, groups = {AddGroup.class, UpdateGroup.class})
    private String classroomType;
}
