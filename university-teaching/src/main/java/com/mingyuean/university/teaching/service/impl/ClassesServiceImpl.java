package com.mingyuean.university.teaching.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.Query;
import com.mingyuean.university.teaching.dao.ClassesDao;
import com.mingyuean.university.teaching.dao.ProfessionalDao;
import com.mingyuean.university.teaching.entity.ClassesEntity;
import com.mingyuean.university.teaching.entity.ProfessionalEntity;
import com.mingyuean.university.teaching.service.ClassesService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service("classesService")
public class ClassesServiceImpl extends ServiceImpl<ClassesDao, ClassesEntity> implements ClassesService {
    @Autowired
    private ProfessionalDao professionalDao;

    @Override
    public List<ClassesEntity> init(List<ClassesEntity> list) {
        //id数组
        List<Long> professionalIdList = new ArrayList<>();
        //添加id
        list.forEach(item -> {
            professionalIdList.add(item.getProfessionalId());
        });
        //根据id生成的列表
        final List<ProfessionalEntity> professionalEntityList = professionalDao.selectBatchIds(professionalIdList);
        //设置实体
        for (ClassesEntity classesEntity : list) {
            professionalEntityList.forEach(item -> {
                if (item.getId().equals(classesEntity.getProfessionalId())) {
                    classesEntity.setProfessionalEntity(item);
                }
            });
        }
        list.forEach(System.out::println);
        return list;
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        QueryWrapper<ClassesEntity> queryWrapper = new QueryWrapper<>();
        //获取key
        String key = (String) params.get("key");
        if (StringUtils.isNotEmpty(key)) {
            //模糊查询
            queryWrapper.and((item) -> {
                item.eq("id", key);
            }).or((item) -> {
                item.like("classes_name", key);
            });
        }

        IPage<ClassesEntity> page = this.page(
                new Query<ClassesEntity>().getPage(params),
                queryWrapper
        );
        page.setRecords(init(page.getRecords()));
        return new PageUtils(page);
    }

}