package com.mingyuean.university.teaching.controller;

import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.R;
import com.mingyuean.common.validator.group.AddGroup;
import com.mingyuean.common.validator.group.UpdateGroup;
import com.mingyuean.university.teaching.entity.ClassesEntity;
import com.mingyuean.university.teaching.service.ClassesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 班级
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 17:48:54
 */
@RefreshScope
@RestController
@RequestMapping("teaching/classes")
public class ClassesController {

    @Autowired
    private ClassesService classesService;

    /**
     * 查询所有班级
     */
    @RequestMapping("/findAll")
    @Cacheable(value = {"Classes"},key =  "#root.methodName",sync = true)
    //@RequiresPermissions("teaching:classes:list")
    public R findAll() {
        final List<ClassesEntity> list = classesService.list();

        return R.ok().put("classes", list);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("teaching:classes:list")
    public R list(@RequestParam Map<String, Object> params) {
        params.remove("t");
        PageUtils page = classesService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("teaching:classes:info")
    public R info(@PathVariable("id") Long id) {
        ClassesEntity classes = classesService.getById(id);

        return R.ok().put("classes", classes);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @CacheEvict(value = {"Classes"}, allEntries = true)
    //@RequiresPermissions("teaching:classes:save")
    public R save(@Validated({AddGroup.class}) @RequestBody ClassesEntity classes) {
        classesService.save(classes);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @CacheEvict(value = {"Classes"}, allEntries = true)
    //@RequiresPermissions("teaching:classes:update")
    public R update(@Validated({UpdateGroup.class}) @RequestBody ClassesEntity classes) {
        classesService.updateById(classes);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @CacheEvict(value = {"Classes"}, allEntries = true)
    //@RequiresPermissions("teaching:classes:delete")
    public R delete(@RequestBody Long[] ids) {
        classesService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
