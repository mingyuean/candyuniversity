package com.mingyuean.university.teaching.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mingyuean.university.teaching.entity.ClassesEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 班级
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 17:48:54
 */
@Mapper
public interface ClassesDao extends BaseMapper<ClassesEntity> {

}
