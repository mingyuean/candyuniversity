package com.mingyuean.university.teaching.aop;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mingyuean.common.exception.RRException;
import com.mingyuean.common.exception.ResultCode;
import com.mingyuean.university.teaching.dao.ClassesCourseTeacherDao;
import com.mingyuean.university.teaching.entity.ClassesCourseTeacherEntity;
import com.mingyuean.university.teaching.entity.ClassesEntity;
import com.mingyuean.university.teaching.entity.TeacherEntity;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author MingYueAn
 * @Description: 修改
 * @Date: 2022/5/29 13:48
 * @Version: 1.0
 */
@Component
@Aspect
public class UpdateAspect {
    @Autowired
    private ClassesCourseTeacherDao classesCourseTeacherDao;
    QueryWrapper<ClassesCourseTeacherEntity> queryWrapper = new QueryWrapper<>();

    /**
     * 关键字：execution(表达式)
     * <p>
     * 表达式： 访问修饰符 返回值 包名.包名.包名…类名.方法名(参数列表)
     */
    @Pointcut("execution(* com.mingyuean.university.teaching.controller.*.update(..))")
    public void pointCut() {
    }

    @Before(value = "pointCut()")
    public void before(JoinPoint joinPoint) {
        System.out.println("类名 = " + joinPoint.getSignature().getDeclaringTypeName());
        System.out.println("当前方法名：" + joinPoint.getSignature().getName());
        System.out.println("当前执行方法参数 = " + joinPoint.getArgs()[0]);
        switch (joinPoint.getSignature().getDeclaringTypeName()) {
            //班级
            case "com.mingyuean.university.teaching.controller.ClassesController": {
                final ClassesEntity arg = (ClassesEntity) joinPoint.getArgs()[0];
                queryWrapper.eq("classes_id", arg.getId());
                //查找分课记录中存在多少班级课程记录
                final int classesCount = Math.toIntExact(classesCourseTeacherDao.selectCount(queryWrapper));
                //分课记录数<=当前班级每周课程上限
                if (classesCount <= arg.getClassesWeekMax()) {
                    break;
                } else {
                    throw new RRException("请先删除分课记录", ResultCode.UPDATE_EXCEPTION.getCode());
                }
            }
            //教师
            case "com.mingyuean.university.teaching.controller.TeacherController": {
                final TeacherEntity arg = (TeacherEntity) joinPoint.getArgs()[0];
                queryWrapper.eq("teacher_id", arg.getId());
                //查找分课记录中存在多少教授课程
                final int teacherCount = Math.toIntExact(classesCourseTeacherDao.selectCount(queryWrapper));
                //分课记录数<=当前教师课程上限
                if (teacherCount <= arg.getTeacherWeekMax()) {
                    break;
                } else {
                    throw new RRException("请先删除分课记录", ResultCode.UPDATE_EXCEPTION.getCode());
                }
            }
            default:
                System.out.println("修改无前置通知");
                break;
        }
    }
}
