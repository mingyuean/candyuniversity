package com.mingyuean.university.teaching.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.university.teaching.entity.SemesterEntity;

import java.util.Map;

/**
 * @author MingYueAn
 * @Description:
 * @Date: 2022/5/24 16:27
 * @Version: 1.0
 */
public interface SemesterService extends IService<SemesterEntity> {
    PageUtils queryPage(Map<String, Object> params);
}
