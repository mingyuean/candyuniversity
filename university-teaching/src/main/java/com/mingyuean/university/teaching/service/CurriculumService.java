package com.mingyuean.university.teaching.service;

import com.mingyuean.university.teaching.entity.Curriculum;

import java.util.List;
import java.util.Map;

/**
 * @author MingYueAn
 * @Description: 排课
 * @Date: 2022/5/20 20:45
 * @Version: 1.0
 */
public interface CurriculumService {
    /**
     * 查看课程表
     *
     * @param params 前端传参
     * @return
     */
    List<Curriculum> findAllByClassesOrTeacher(Map<String, Object> params);

    /**
     * 排课
     *
     * @return
     */
    void timeTable();

}
