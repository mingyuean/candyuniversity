package com.mingyuean.university.teaching.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mingyuean.university.teaching.entity.ProfessionalEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 专业
 * @author MingYueAn
 * @Description: 专业
 * @Date: 2022/5/29 15:55
 * @Version: 1.0
 */
@Mapper
public interface ProfessionalDao extends BaseMapper<ProfessionalEntity> {
}
