package com.mingyuean.university.teaching.controller;

import com.mingyuean.common.utils.R;
import com.mingyuean.university.teaching.entity.Curriculum;
import com.mingyuean.university.teaching.service.CurriculumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <p>排课</p>
 *
 * @author MingYueAn
 * @Description: 排课
 * @Date: 2022/5/20 20:43
 * @Version: 1.0
 */
@RestController
@RequestMapping("teaching/curriculum")
public class CurriculumController {
    @Autowired
    private CurriculumService curriculumService;

    /**
     * 查看课程表
     *
     * @param params
     * @return
     */
    @RequestMapping("/find_curriculum")
    //@RequiresPermissions("teaching:classes:list")
    public R findAll(@RequestParam Map<String, Object> params) {
        params.remove("t");
        final List<Curriculum> list = curriculumService.findAllByClassesOrTeacher(params);

        return R.ok().put("curriculum", list);
    }

    /**
     * 开始排课
     */
    @RequestMapping("/time_table")
    //@RequiresPermissions("teaching:classes:list")
    public R timeTable() {
        curriculumService.timeTable();
        return R.ok();
    }

}
