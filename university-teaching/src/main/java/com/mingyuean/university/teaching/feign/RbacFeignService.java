package com.mingyuean.university.teaching.feign;

import com.mingyuean.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 远程调用权限管理服务
 * <p>
 * 在@FeignClient使用自定义的服务名
 * <p>
 * 声明式的远程调用
 *
 * @Author: MingYueAn
 * @Description: 远程调用权限管理服务
 * @Date: 2022/5/8 22:09
 * @Version: 1.0
 */
@FeignClient("university-rbac")
public interface RbacFeignService {
    /**
     * 远程调用
     * 获取用户信息
     *
     * @return
     */
    @RequestMapping("rbac/user/info/{id}")
    public R info(@PathVariable("id") Long id);
}
