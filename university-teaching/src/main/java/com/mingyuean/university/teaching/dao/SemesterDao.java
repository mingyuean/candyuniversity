package com.mingyuean.university.teaching.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mingyuean.university.teaching.entity.SemesterEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author MingYueAn
 * @Description: 学期
 * @Date: 2022/5/24 16:27
 * @Version: 1.0
 */
@Mapper
public interface SemesterDao extends BaseMapper<SemesterEntity> {
}
