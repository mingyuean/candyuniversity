package com.mingyuean.university.teaching.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.Query;
import com.mingyuean.university.teaching.dao.ProfessionalDao;
import com.mingyuean.university.teaching.entity.ProfessionalEntity;
import com.mingyuean.university.teaching.service.ProfessionalService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("professionalService")
public class ProfessionalServiceImpl extends ServiceImpl<ProfessionalDao, ProfessionalEntity> implements ProfessionalService{

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<ProfessionalEntity> queryWrapper = new QueryWrapper<>();
        //获取key
        String key = (String) params.get("key");
        if (StringUtils.isNotEmpty(key)) {
            //模糊查询
            queryWrapper.and((item) -> {
                item.eq("id", key);
            }).or((item) -> {
                item.like("professional_name", key);
            });
        }

        IPage<ProfessionalEntity> page = this.page(
                new Query<ProfessionalEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

}