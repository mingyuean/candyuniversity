package com.mingyuean.university.teaching.aop;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mingyuean.common.exception.RRException;
import com.mingyuean.common.exception.ResultCode;
import com.mingyuean.university.teaching.dao.*;
import com.mingyuean.university.teaching.entity.*;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 保存、修改
 *
 * @author MingYueAn
 * @Description: 保存\修改
 * @Date: 2022/5/27 16:21
 * @Version: 1.0
 */
@Component
@Aspect
public class SaveAspect {
    //https://blog.csdn.net/worilb/article/details/114156126
    //https://blog.csdn.net/Mr_97xu/article/details/115795608

    @Autowired
    private ClassesDao classesDao;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private TeacherDao teacherDao;
    @Autowired
    private ClassroomDao classroomDao;
    @Autowired
    private ProfessionalDao professionalDao;
    @Autowired
    private ClassesCourseTeacherDao classesCourseTeacherDao;

    @Pointcut("execution(* com.mingyuean.university.teaching.controller.*.save(..))")
    public void pointCutSave() {
    }

    @Pointcut("execution(* com.mingyuean.university.teaching.controller.*.update(..))")
    public void pointCutUpdate() {
    }

    /**
     * 关键字：execution(表达式)
     * <p>
     * 表达式： 访问修饰符 返回值 包名.包名.包名…类名.方法名(参数列表)
     */
    @Pointcut("pointCutUpdate()||pointCutSave()")
    public void pointCut() {
    }

    @Before(value = "pointCut()")
    public void before(JoinPoint joinPoint) {
        System.out.println("类名 = " + joinPoint.getSignature().getDeclaringTypeName());
        System.out.println("当前方法名：" + joinPoint.getSignature().getName());
        System.out.println("当前执行方法参数 = " + joinPoint.getArgs()[0]);
        String method = "update";
        switch (joinPoint.getSignature().getDeclaringTypeName()) {
            //班级
            case "com.mingyuean.university.teaching.controller.ClassesController": {
                final ClassesEntity arg = (ClassesEntity) joinPoint.getArgs()[0];
                final QueryWrapper<ClassesEntity> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("classes_name", arg.getClassesName());
                judgeMethod(joinPoint, method, classesDao.selectCount(queryWrapper), false);
                break;
            }
            //教室
            case "com.mingyuean.university.teaching.controller.ClassroomController": {
                final ClassroomEntity arg = (ClassroomEntity) joinPoint.getArgs()[0];
                final QueryWrapper<ClassroomEntity> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("classroom_name", arg.getClassroomName());
                judgeMethod(joinPoint, method, classroomDao.selectCount(queryWrapper), false);
                break;
            }
            //课程
            case "com.mingyuean.university.teaching.controller.CourseController": {
                final CourseEntity arg = (CourseEntity) joinPoint.getArgs()[0];
                final QueryWrapper<CourseEntity> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("course_name", arg.getCourseName());
                judgeMethod(joinPoint, method, courseDao.selectCount(queryWrapper), false);
                break;
            }
            //教师
            case "com.mingyuean.university.teaching.controller.TeacherController": {
                final TeacherEntity arg = (TeacherEntity) joinPoint.getArgs()[0];
                final QueryWrapper<TeacherEntity> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("teacher_name", arg.getTeacherName());
                judgeMethod(joinPoint, method, teacherDao.selectCount(queryWrapper), false);
                break;
            }
            //专业
            case "com.mingyuean.university.teaching.controller.ProfessionalController": {
                final ProfessionalEntity arg = (ProfessionalEntity) joinPoint.getArgs()[0];
                final QueryWrapper<ProfessionalEntity> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("professional_name", arg.getProfessionalName());
                judgeMethod(joinPoint, method, professionalDao.selectCount(queryWrapper), false);
                break;
            }
            //分课
            case "com.mingyuean.university.teaching.controller.ClassesCourseTeacherController": {
                final ClassesCourseTeacherEntity arg = (ClassesCourseTeacherEntity) joinPoint.getArgs()[0];
                final QueryWrapper<ClassesCourseTeacherEntity> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("classes_id", arg.getClassesId());
                queryWrapper.eq("course_id", arg.getCourseId());
                judgeMethod(joinPoint, method, classesCourseTeacherDao.selectCount(queryWrapper), true);
                break;
            }
            default:
                System.out.println("保存无前置通知");
                break;
        }
    }

    private void judgeMethod(JoinPoint joinPoint, String method, Long aLong, boolean b) {
        if (joinPoint.getSignature().getName().equals(method)) {
            if (aLong != 0 && aLong != 1) {
                if (b) {
                    throw new RRException("分课记录重复", ResultCode.SAVE_EXCEPTION.getCode());
                } else {
                    throw new RRException("名称重复", ResultCode.SAVE_EXCEPTION.getCode());
                }
            }
        } else {
            if (aLong != 0) {
                if (b) {
                    throw new RRException("分课记录重复", ResultCode.SAVE_EXCEPTION.getCode());
                } else {
                    throw new RRException("名称重复", ResultCode.SAVE_EXCEPTION.getCode());
                }
            }
        }
    }

}
