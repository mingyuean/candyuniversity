package com.mingyuean.university.teaching.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mingyuean.common.validator.group.AddGroup;
import com.mingyuean.common.validator.group.UpdateGroup;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;

/**
 * 课程
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 17:48:54
 */
@Data
@TableName("tcg_course")
public class CourseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 序号
     */
    @NotNull(message = "修改必须指定id", groups = {UpdateGroup.class})
    @Null(message = "新增不能指定id", groups = {AddGroup.class})
    @TableId
    private Long id;
    /**
     * 名字
     */
    @NotBlank(message = "请输入课程名", groups = {AddGroup.class})
    @Length(max = 10, message = "课程名不能超过个 {max} 字符", groups = {AddGroup.class, UpdateGroup.class})
    private String courseName;
    /**
     * 教材
     */
    @NotBlank(message = "请输入教材", groups = {AddGroup.class})
    @Length(max = 10, message = "教材名不能超过个 {max} 字符", groups = {AddGroup.class, UpdateGroup.class})
    private String courseBook;
    /**
     * 课程类型
     */
    @NotBlank(message = "请输入课程类型", groups = {AddGroup.class})
//    @ListValue(values = {'0', '1', 'A', 'B'}, groups = {AddGroup.class, UpdateGroup.class})
    private String courseType;

}
