package com.mingyuean.university.teaching.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mingyuean.common.validator.ListValue;
import com.mingyuean.common.validator.group.AddGroup;
import com.mingyuean.common.validator.group.UpdateGroup;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author MingYueAn
 * @Description:
 * @Date: 2022/5/24 16:27
 * @Version: 1.0
 */
@Data
@TableName("tcg_semester")
public class SemesterEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotNull(message = "修改必须指定id", groups = {UpdateGroup.class})
    @Null(message = "新增不能指定id", groups = {AddGroup.class})
    @TableId
    private Long id;
    private LocalDate semesterStartDate;
    private LocalDate semesterEndDate;
    @NotNull(message = "请输入学期数", groups = {AddGroup.class})
    @Range(min = 1, max = 20, message = "学期数范围为 {min} 到 {max} 之间", groups = {AddGroup.class, UpdateGroup.class})
    private Integer semesterOrder;
    @Null(message = "是否是当前学期（0不是，1是）应当为空", groups = {AddGroup.class})
    @ListValue(values = {0, 1}, groups = {AddGroup.class, UpdateGroup.class})
    private Integer semesterYesNo;

    /**
     * 非数据库表中字段
     * 时间段
     */
    @TableField(exist = false)
    private String datetime;
}
