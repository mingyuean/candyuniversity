package com.mingyuean.university.teaching.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mingyuean.common.validator.group.AddGroup;
import com.mingyuean.common.validator.group.UpdateGroup;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;

/**
 * 班级
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 17:48:54
 */
@Data
@TableName("tcg_classes")
public class ClassesEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 序号
     */
    @NotNull(message = "修改必须指定id", groups = {UpdateGroup.class})
    @Null(message = "新增不能指定id", groups = {AddGroup.class})
    @TableId
    private Long id;
    /**
     * 名字
     */
    @NotBlank(message = "请输入班级名", groups = {AddGroup.class})
    @Length(max = 10, message = "班级名不能超过个 {max} 字符", groups = {AddGroup.class, UpdateGroup.class})
    private String classesName;
    /**
     * 人数
     */
    @NotNull(message = "请输入班级人数", groups = {AddGroup.class})
    @Range(min = 20, max = 50, message = "班级人数范围为 {min} 到 {max} 之间", groups = {AddGroup.class, UpdateGroup.class})
    private Integer classesNumber;
    /**
     * 每周课程上限
     */
    @NotNull(message = "请输入每周课程上限", groups = {AddGroup.class})
    @Range(min = 0, max = 25, message = "每周课程上限范围为 {min} 到 {max} 之间", groups = {AddGroup.class, UpdateGroup.class})
    private Integer classesWeekMax;
    /**
     * 专业id
     */
    private Long professionalId;
    /**
     * 非数据库表中字段
     * 专业实体：获取专业信息
     */
    @TableField(exist = false)
    private ProfessionalEntity professionalEntity;

}
