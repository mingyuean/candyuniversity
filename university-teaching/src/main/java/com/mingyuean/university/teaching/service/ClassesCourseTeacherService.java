package com.mingyuean.university.teaching.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.R;
import com.mingyuean.university.teaching.entity.ClassesCourseTeacherEntity;

import java.util.List;
import java.util.Map;

/**
 * 班级课程教师关联表（分配课程，为排课做准备）
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-15 09:44:25
 */
public interface ClassesCourseTeacherService extends IService<ClassesCourseTeacherEntity> {


    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    PageUtils queryPage(Map<String, Object> params);

    /**
     * 添加（添加前判断教师课程上限制）
     *
     * @param classesCourseTeacher
     * @return
     */
    R saveByDetail(ClassesCourseTeacherEntity classesCourseTeacher);

    /**
     * 修改详细信息（id、name要一起修改）
     *
     * @param classesCourseTeacher
     */
    R updateByIdDetail(ClassesCourseTeacherEntity classesCourseTeacher);

    List<ClassesCourseTeacherEntity> init(List<ClassesCourseTeacherEntity> selectList);
}

