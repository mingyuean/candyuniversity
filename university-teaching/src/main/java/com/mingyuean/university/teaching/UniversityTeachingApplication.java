package com.mingyuean.university.teaching;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@EnableFeignClients(basePackages = "com.mingyuean.university.teaching.feign")
@EnableDiscoveryClient
@EnableAspectJAutoProxy
@SpringBootApplication
public class UniversityTeachingApplication {

    public static void main(String[] args) {
        SpringApplication.run(UniversityTeachingApplication.class, args);
    }

}
