package com.mingyuean.university.teaching.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.university.teaching.entity.ClassroomEntity;

import java.util.Map;

/**
 * 教室
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 17:48:54
 */
public interface ClassroomService extends IService<ClassroomEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

