package com.mingyuean.university.teaching.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mingyuean.university.teaching.dao.*;
import com.mingyuean.university.teaching.entity.ClassesEntity;
import com.mingyuean.university.teaching.entity.ClassroomEntity;
import com.mingyuean.university.teaching.entity.CourseEntity;
import com.mingyuean.university.teaching.service.ClassesService;
import com.mingyuean.university.teaching.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author MingYueAn
 * @Description: 统计
 * @Date: 2022/6/2 10:03
 * @Version: 1.0
 */
@Service
public class StatisticsServiceImpl implements StatisticsService {
    @Autowired
    private ClassesDao classesDao;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private TeacherDao teacherDao;
    @Autowired
    private ClassroomDao classroomDao;
    @Autowired
    private ProfessionalDao professionalDao;
    @Autowired
    private ClassesService classesService;

    @Override
    public List<Integer> findCount() {
        final ArrayList<Integer> countList = new ArrayList<>(4);

        final Integer count1 = Math.toIntExact(professionalDao.selectCount(new QueryWrapper<>()));
        countList.add(count1);
        final Integer count2 = Math.toIntExact(classesDao.selectCount(new QueryWrapper<>()));
        countList.add(count2);
        final Integer count3 = Math.toIntExact(courseDao.selectCount(new QueryWrapper<>()));
        countList.add(count3);
        final Integer count4 = Math.toIntExact(teacherDao.selectCount(new QueryWrapper<>()));
        countList.add(count4);

        return countList;
    }

    @Override
    public List<Integer> findCourseTypeCount() {
        final ArrayList<Integer> countList = new ArrayList<>(2);

        final QueryWrapper<CourseEntity> queryWrapper0 = new QueryWrapper<>();
        queryWrapper0.eq("course_type", "0");
        final Integer count0 = Math.toIntExact(courseDao.selectCount(queryWrapper0));
        countList.add(count0);
        final QueryWrapper<CourseEntity> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("course_type", "1");
        final Integer count1 = Math.toIntExact(courseDao.selectCount(queryWrapper1));
        countList.add(count1);

        return countList;
    }

    @Override
    public List<Integer> findClassroomTypeCount() {
        final ArrayList<Integer> countList = new ArrayList<>(3);

        final QueryWrapper<ClassroomEntity> queryWrapper0 = new QueryWrapper<>();
        queryWrapper0.eq("classroom_type", "0");
        final Integer count0 = Math.toIntExact(classroomDao.selectCount(queryWrapper0));
        countList.add(count0);

        final QueryWrapper<ClassroomEntity> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("classroom_type", "1");
        final Integer count1 = Math.toIntExact(classroomDao.selectCount(queryWrapper1));
        countList.add(count1);

        final QueryWrapper<ClassroomEntity> queryWrapperA = new QueryWrapper<>();
        queryWrapperA.eq("classroom_type", "A");
        final Integer countA = Math.toIntExact(classroomDao.selectCount(queryWrapperA));
        countList.add(countA);

        return countList;
    }

    @Override
    public List<Integer> findProfessionalTypeCount() {
        int[] arr = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        final List<ClassesEntity> classesEntityList = classesService.init(classesDao.selectList(new QueryWrapper<>()));
        classesEntityList.forEach(item -> {
            switch (item.getProfessionalEntity().getProfessionalType()) {
                case "a":
                    arr[0]++;
                    break;
                case "b":
                    arr[1]++;
                    break;
                case "c":
                    arr[2]++;
                    break;
                case "d":
                    arr[3]++;
                    break;
                case "e":
                    arr[4]++;
                    break;
                case "f":
                    arr[5]++;
                    break;
                case "g":
                    arr[6]++;
                    break;
                case "h":
                    arr[7]++;
                    break;
                case "i":
                    arr[8]++;
                    break;
                case "j":
                    arr[8]++;
                    break;
                case "k":
                    arr[10]++;
                    break;
                case "l":
                    arr[11]++;
                    break;
                default:
                    break;
            }
        });
        // int[] 转 List<Integer>
        // Arrays.stream将int[]转化为IntStream
        // boxed()装箱，将IntStream转化为Stream<Integer>
        // collect将Stream<Integer>转化为List<Integer>
        List<Integer> list = Arrays.stream(arr).boxed().collect(Collectors.toList());
        return list;
    }
}
