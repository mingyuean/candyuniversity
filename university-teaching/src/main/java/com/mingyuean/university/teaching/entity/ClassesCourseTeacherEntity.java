package com.mingyuean.university.teaching.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 班级课程教师关联表（分配课程，为排课做准备）
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-15 09:44:25
 */
@Data
@TableName("tcg_classes_course_teacher")
public class ClassesCourseTeacherEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 序号
     */
    @TableId
    private Long id;
    /**
     * 班级id
     */
    private Long classesId;
    /**
     * 课程id
     */
    private Long courseId;
    /**
     * 教师id
     */
    private Long teacherId;
    /**
     * 星期 ABCDE
     * 课 01、02、03、04、05、06、07、08、09、10
     */
    private String weekNumber;
    /**
     * 随机分配教室id
     */
    private Long classroomId;

    /**
     * 非数据库表中字段
     * 班级实体：获取班级信息
     */
    @TableField(exist = false)
    private ClassesEntity classesEntity;
    /**
     * 非数据库表中字段
     * 课程实体：获取课程信息
     */
    @TableField(exist = false)
    private CourseEntity courseEntity;
    /**
     * 非数据库表中字段
     * 教师实体：获取教师信息
     */
    @TableField(exist = false)
    private TeacherEntity teacherEntity;
    /**
     * 非数据库表中字段
     * 教室实体：获取教室信息
     */
    @TableField(exist = false)
    private ClassroomEntity classroomEntity;
    /**
     * 非数据库表中字段
     * 数据解析后的时间
     */
    @TableField(exist = false)
    private String time;
}
