package com.mingyuean.university.teaching.aop;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mingyuean.common.exception.RRException;
import com.mingyuean.common.exception.ResultCode;
import com.mingyuean.university.teaching.dao.ClassesCourseTeacherDao;
import com.mingyuean.university.teaching.dao.ClassesDao;
import com.mingyuean.university.teaching.entity.ClassesCourseTeacherEntity;
import com.mingyuean.university.teaching.entity.ClassesEntity;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 删除
 *
 * @author MingYueAn
 * @Description: 删除
 * @Date: 2022/5/29 13:34
 * @Version: 1.0
 */
@Component
@Aspect
public class DeleteAspect {

    @Autowired
    private ClassesCourseTeacherDao classesCourseTeacherDao;
    final QueryWrapper<ClassesCourseTeacherEntity> queryWrapper = new QueryWrapper<>();
    @Autowired
    private ClassesDao classesDao;
    final QueryWrapper<ClassesEntity> classesQueryWrapper  = new QueryWrapper<>();

    /**
     * 关键字：execution(表达式)
     * <p>
     * 表达式： 访问修饰符 返回值 包名.包名.包名…类名.方法名(参数列表)
     */
    @Pointcut("execution(* com.mingyuean.university.teaching.controller.*.delete(..))")
    public void pointCut() {
    }

    @Before(value = "pointCut()")
    public void before(JoinPoint joinPoint) {
        //id集合
        Long[] ids = (Long[]) joinPoint.getArgs()[0];
        List<Long> list = new ArrayList<>(Arrays.asList(ids));

        System.out.println("类名 = " + joinPoint.getSignature().getDeclaringTypeName());
        System.out.println("当前方法名：" + joinPoint.getSignature().getName());
        switch (joinPoint.getSignature().getDeclaringTypeName()) {
            //班级
            case "com.mingyuean.university.teaching.controller.ClassesController": {
                queryWrapper.in("classes_id", list);
                if (classesCourseTeacherDao.selectCount(queryWrapper) != 0) {
                    throw new RRException("存在分课记录，无法删除该班级", ResultCode.DELETE_EXCEPTION.getCode());
                } else {
                    break;
                }
            }
            //教室
            case "com.mingyuean.university.teaching.controller.ClassroomController": {
                queryWrapper.in("classroom_id", list);
                if (classesCourseTeacherDao.selectCount(queryWrapper) != 0) {
                    throw new RRException("存在分课记录，无法删除该教室", ResultCode.DELETE_EXCEPTION.getCode());
                } else {
                    break;
                }
            }
            //课程
            case "com.mingyuean.university.teaching.controller.CourseController": {
                queryWrapper.in("course_id", list);
                if (classesCourseTeacherDao.selectCount(queryWrapper) != 0) {
                    throw new RRException("存在分课记录，无法删除该课程", ResultCode.DELETE_EXCEPTION.getCode());
                } else {
                    break;
                }
            }
            //教师
            case "com.mingyuean.university.teaching.controller.TeacherController": {
                queryWrapper.in("teacher_id", list);
                if (classesCourseTeacherDao.selectCount(queryWrapper) != 0) {
                    throw new RRException("存在分课记录，无法删除该教师", ResultCode.DELETE_EXCEPTION.getCode());
                } else {
                    break;
                }
            }
            //专业
            case "com.mingyuean.university.teaching.controller.ProfessionalController": {
                classesQueryWrapper.in("professional_id", list);
                if (classesDao.selectCount(classesQueryWrapper) != 0) {
                    throw new RRException("存在班级记录，无法删除该专业", ResultCode.DELETE_EXCEPTION.getCode());
                } else {
                    break;
                }
            }
            default:
                System.out.println("删除无前置通知");
                break;
        }
    }
}
