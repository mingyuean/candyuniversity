package com.mingyuean.university.teaching.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.mingyuean.university.teaching.dao.ClassesCourseTeacherDao;
import com.mingyuean.university.teaching.dao.ClassroomDao;
import com.mingyuean.university.teaching.entity.ClassesCourseTeacherEntity;
import com.mingyuean.university.teaching.entity.ClassroomEntity;
import com.mingyuean.university.teaching.entity.Curriculum;
import com.mingyuean.university.teaching.service.ClassesCourseTeacherService;
import com.mingyuean.university.teaching.service.CurriculumService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;

/**
 * @author MingYueAn
 * @Description: 排课
 * @Date: 2022/5/20 20:45
 * @Version: 1.0
 */
@Service
public class CurriculumServiceImpl implements CurriculumService {
    @Autowired
    private ClassroomDao classroomDao;
    @Autowired
    private ClassesCourseTeacherDao classesCourseTeacherDao;
    @Autowired
    private ClassesCourseTeacherService classesCourseTeacherService;
    /**
     * 排课处理
     */
    private final Set<Map<String, String>> hashSet = new HashSet<>();

    final int week = 5;
    final int list_size = 25;

    /**
     * 查看课程表
     *
     * @param params 前端传过来的值
     * @return
     */
    @Cacheable(value = {"Curriculum"}, key = "#a0", sync = true)
    @Override
    public List<Curriculum> findAllByClassesOrTeacher(Map<String, Object> params) {
//        log.error("缓存测试");
        QueryWrapper<ClassesCourseTeacherEntity> queryWrapper = new QueryWrapper<>();
        //获取key
        String classesId = (String) params.get("classesId");
        String teacherId = (String) params.get("teacherId");
        //查询
        if (StringUtils.isNotEmpty(classesId)) {
            queryWrapper.eq("classes_id", classesId);
        } else if (StringUtils.isNotEmpty(teacherId)) {
            queryWrapper.eq("teacher_id", teacherId);
        }
        //初始化课表
        List<Curriculum> curriculumList = new ArrayList<>();
        for (int i = 1; i <= week; i++) {
            final Curriculum curriculum = new Curriculum();
            curriculum.setCourse("第" + i + "节");
            curriculumList.add(curriculum);
        }
        List<ClassesCourseTeacherEntity> list = classesCourseTeacherService.init(classesCourseTeacherDao.selectList(queryWrapper));
        //判断是否存在分课记录
        if (list.size() == 0) {
            list = null;
        }
        Assert.notNull(list, "请先添加分课记录");
        //分课记录遍历
        for (ClassesCourseTeacherEntity classesCourseTeacherEntity : list) {
            //判断是否进行了排课
            Assert.notNull(classesCourseTeacherEntity.getWeekNumber(), "请先进行排课");
            //获取时间的char数组
            char[] weekNumber = classesCourseTeacherEntity.getWeekNumber().toCharArray();
            //数据解析
            analysisWeekNumber(weekNumber, curriculumList, classesCourseTeacherEntity);
        }
        //课表
        System.out.println("curriculumList = " + curriculumList);
        curriculumList.forEach(System.out::println);
        return curriculumList;
    }

    /**
     * 排课
     */
    @CacheEvict(value = {"Curriculum"}, allEntries = true)
    @Override
    public void timeTable() {
        //条件修改
        final UpdateWrapper<ClassesCourseTeacherEntity> updateWrapper = new UpdateWrapper<>();
        //全部置空
        updateWrapper.set("week_number", null);
        updateWrapper.set("classroom_id", null);
        //修改表
        classesCourseTeacherDao.update(null, updateWrapper);

        //查询所有分课记录
        List<ClassesCourseTeacherEntity> list = classesCourseTeacherService.init(classesCourseTeacherDao.selectList(new QueryWrapper<>()));
        if (list.size() == 0) {
            list = null;
        }
        Assert.notNull(list, "请先添加分课记录");
        //查询所有教室记录
        List<ClassroomEntity> classroomList = classroomDao.selectList(new QueryWrapper<>());
        if (classroomList.size() == 0) {
            classroomList = null;
        }
        Assert.notNull(classroomList, "请先添加教室");
        //循环遍历
        for (ClassesCourseTeacherEntity classesCourseTeacherEntity : list) {
//            log.warn("当前分课id：{}", classesCourseTeacherEntity.getId());
            //可用教室列表
            List<ClassroomEntity> okClassroomList = okClassroomList(classroomList, classesCourseTeacherEntity.getClassesEntity().getClassesNumber(), classesCourseTeacherEntity.getCourseEntity().getCourseType());
//            log.info("可用教室列表：{}", okClassroomList);
            //okClassroomList=null，可用教室列表为空
            Assert.notNull(okClassroomList, "教室空间不足，不符合班级人数要求");
            //时间冲突，分配教室，分配时间
            Map<String, String> map = byWeekNumberAndClassroomId(okClassroomList, new HashSet<>());
            //map=null，教室不够
            Assert.notNull(map, "教室数量不足，请增加教室");

            System.out.println("时间：" + map.get("week_number"));
            System.out.println("教室：" + map.get("classroom_id"));

            classesCourseTeacherEntity.setWeekNumber(map.get("week_number"));
            classesCourseTeacherEntity.setClassroomId(Long.valueOf(map.get("classroom_id")));
            //保存修改后的记录
            classesCourseTeacherDao.updateById(classesCourseTeacherEntity);
        }
        list.forEach(System.out::println);
    }

    /**
     * 数据解析
     *
     * @param weekNumber     时间 字符数组
     * @param curriculumList 课程表
     * @param entity         课程名
     */
    private void analysisWeekNumber(char[] weekNumber, List<Curriculum> curriculumList, ClassesCourseTeacherEntity entity) {
        //数据处理
        switch (weekNumber[0]) {
            case 'A':
                curriculumList.get(Integer.parseInt(String.valueOf(weekNumber[1])) - 1).setOne(entity);
                break;
            case 'B':
                curriculumList.get(Integer.parseInt(String.valueOf(weekNumber[1])) - 1).setTwo(entity);
                break;
            case 'C':
                curriculumList.get(Integer.parseInt(String.valueOf(weekNumber[1])) - 1).setThree(entity);
                break;
            case 'D':
                curriculumList.get(Integer.parseInt(String.valueOf(weekNumber[1])) - 1).setFour(entity);
                break;
            case 'E':
                curriculumList.get(Integer.parseInt(String.valueOf(weekNumber[1])) - 1).setFive(entity);
                break;
            default:
                break;
        }
    }

    /**
     * 排课，分配随机时间，分配随机教室，解决教室时间冲突
     *
     * @param okClassroomList 可用教室列表
     * @param errorSet        冲突遍历
     * @return map=时间+教室
     */
    private Map<String, String> byWeekNumberAndClassroomId(List<ClassroomEntity> okClassroomList, HashSet<Object> errorSet) {
//        log.info("冲突遍历{}", errorSet);
        Map<String, String> map = new HashMap<>(2);
        //随机时间
        map.put("week_number", randomWeekNumber());
//        log.info("随机时间 = {}", map.get("week_number"));
        //随机可用教室
        map.put("classroom_id", randomClassroomEntity(okClassroomList).toString());
//        log.info("随机教室 = {}", map.get("classroom_id"));
        if (hashSet.add(map)) {
//            log.info("无重复");
            return map;
        } else if (errorSet.size() == okClassroomList.size() * list_size) {
//            log.error("时间+教室，完全遍历");
            return null;
        } else {
//            log.error("重复");
            map.replace("week_number", randomWeekNumber());
            map.replace("classroom_id", randomClassroomEntity(okClassroomList).toString());
            errorSet.add(map);
            return byWeekNumberAndClassroomId(okClassroomList, errorSet);
        }
    }

    /**
     * 可用教室列表：将教室大小与班级人数进行比较
     *
     * @param classroomList 全部教室列表
     * @param classesNumber 班级人数
     * @param courseType    课程类型（0理论课，1实验课）
     * @return 可用教室列表
     */
    private List<ClassroomEntity> okClassroomList(List<ClassroomEntity> classroomList, Integer classesNumber, String courseType) {
        List<ClassroomEntity> okClassroomList = new ArrayList<>();
        for (ClassroomEntity classroomEntity : classroomList) {
            if (classroomEntity.getClassroomSize() >= classesNumber) {
                //符合 随机分配的教室大小>=分课记录中班级的人数
                if (classroomEntity.getClassroomType().equals(courseType)) {
                    //类型匹配
                    okClassroomList.add(classroomEntity);
                }
            }
        }
        if (okClassroomList.size() == 0) {
            return null;
        } else {
            return okClassroomList;
        }
    }


    /**
     * 随机可用教室
     *
     * @param okClassroomList 随机可用教室列表
     * @return 随机教室id
     */
    private Long randomClassroomEntity(List<ClassroomEntity> okClassroomList) {
        //随机下标
        int random1 = new Random().nextInt(okClassroomList.size()) + 1;
        //随机教室id
        return okClassroomList.get(random1 - 1).getId();
    }


    /**
     * 随机时间
     *
     * @return 时间字符串
     */
    private String randomWeekNumber() {
        //时间字符串
        String weekNumber = "";
        //星期的随机数
        int x = new Random().nextInt(5) + 1;
        //星期 A B C D E
        switch (x) {
            //星期一
            case 1:
                weekNumber = "A";
                break;
            //星期二
            case 2:
                weekNumber = "B";
                break;
            //星期三
            case 3:
                weekNumber = "C";
                break;
            //星期四
            case 4:
                weekNumber = "D";
                break;
            //星期五
            case 5:
                weekNumber = "E";
                break;
            default:
                break;
        }
        //课的随机数
        int y = new Random().nextInt(5) + 1;
        //课 01、02、03、04、05、06、07、08、09、10
        switch (y) {
            //1：一二节课
            case 1:
                weekNumber = weekNumber + "1:01\\02";
                break;
            //2:三四节课
            case 2:
                weekNumber = weekNumber + "2:03\\04";
                break;
            //3:五六节课
            case 3:
                weekNumber = weekNumber + "3:05\\06";
                break;
            //4:七八节课
            case 4:
                weekNumber = weekNumber + "4:07\\08";
                break;
            //5:九十节课
            case 5:
                weekNumber = weekNumber + "5:09\\10";
                break;
            default:
                break;
        }
        return weekNumber;
    }

}
