package com.mingyuean.university.teaching.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.Query;
import com.mingyuean.university.teaching.dao.TeacherDao;
import com.mingyuean.university.teaching.entity.TeacherEntity;
import com.mingyuean.university.teaching.service.TeacherService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("teacherService")
public class TeacherServiceImpl extends ServiceImpl<TeacherDao, TeacherEntity> implements TeacherService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<TeacherEntity> queryWrapper = new QueryWrapper<>();
        //获取key
        String key = (String) params.get("key");
        if (StringUtils.isNotEmpty(key)) {
            //模糊查询
            queryWrapper.and((item) -> {
                item.eq("id", key);
            }).or((item) -> {
                item.like("teacher_name", key);
            });
        }

        IPage<TeacherEntity> page = this.page(
                new Query<TeacherEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

}