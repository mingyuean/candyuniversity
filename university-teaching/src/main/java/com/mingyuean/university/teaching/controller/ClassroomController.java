package com.mingyuean.university.teaching.controller;

import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.R;
import com.mingyuean.common.validator.group.AddGroup;
import com.mingyuean.common.validator.group.UpdateGroup;
import com.mingyuean.university.teaching.entity.ClassroomEntity;
import com.mingyuean.university.teaching.service.ClassroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 教室
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 17:48:54
 */
@RestController
@RequestMapping("teaching/classroom")
public class ClassroomController {
    @Autowired
    private ClassroomService classroomService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("teaching:classroom:list")
    public R list(@RequestParam Map<String, Object> params) {
        params.remove("t");
        PageUtils page = classroomService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("teaching:classroom:info")
    public R info(@PathVariable("id") Long id) {
        ClassroomEntity classroom = classroomService.getById(id);

        return R.ok().put("classroom", classroom);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("teaching:classroom:save")
    public R save(@Validated({AddGroup.class}) @RequestBody ClassroomEntity classroom) {
        classroomService.save(classroom);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("teaching:classroom:update")
    public R update(@Validated({UpdateGroup.class}) @RequestBody ClassroomEntity classroom) {
        classroomService.updateById(classroom);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("teaching:classroom:delete")
    public R delete(@RequestBody Long[] ids) {
        classroomService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
