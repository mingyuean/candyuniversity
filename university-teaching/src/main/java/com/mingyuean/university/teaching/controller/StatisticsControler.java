package com.mingyuean.university.teaching.controller;

import com.mingyuean.common.utils.R;
import com.mingyuean.university.teaching.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author MingYueAn
 * @Description: 统计
 * @Date: 2022/6/1 16:29
 * @Version: 1.0
 */
@RestController
@RequestMapping("teaching/statistics")
public class StatisticsControler {
    @Autowired
    private StatisticsService statisticsService;

    @RequestMapping("/count")
    //@RequiresPermissions("teaching:classes:list")
    public R findCount() {
        List<Integer> countList = statisticsService.findCount();
        return R.ok().put("count", countList);
    }

    @RequestMapping("/courseTypeCount")
    //@RequiresPermissions("teaching:classes:list")
    public R courseTypeCount() {
        List<Integer> countList = statisticsService.findCourseTypeCount();
        return R.ok().put("count", countList);
    }

    @RequestMapping("/classroomTypeCount")
    //@RequiresPermissions("teaching:classes:list")
    public R classroomTypeCount() {
        List<Integer> countList = statisticsService.findClassroomTypeCount();
        return R.ok().put("count", countList);
    }

    @RequestMapping("/professionalTypeCount")
    //@RequiresPermissions("teaching:classes:list")
    public R professionalTypeCount() {
        List<Integer> countList = statisticsService.findProfessionalTypeCount();
        return R.ok().put("count", countList);
    }


}
