package com.mingyuean.university.teaching.controller;

import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.R;
import com.mingyuean.common.validator.group.AddGroup;
import com.mingyuean.common.validator.group.UpdateGroup;
import com.mingyuean.university.teaching.entity.TeacherEntity;
import com.mingyuean.university.teaching.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 老师
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 17:48:54
 */
@RestController
@RequestMapping("teaching/teacher")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    /**
     * 查询所有教师
     */
    @RequestMapping("/findAll")
    @Cacheable(value = {"Teacher"},key =  "#root.methodName",sync = true)
    //@RequiresPermissions("teaching:classes:list")
    public R findAll() {
        final List<TeacherEntity> list = teacherService.list();

        return R.ok().put("teacher", list);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("teaching:teacher:list")
    public R list(@RequestParam Map<String, Object> params) {
        params.remove("t");
        PageUtils page = teacherService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("teaching:teacher:info")
    public R info(@PathVariable("id") Long id) {
        TeacherEntity teacher = teacherService.getById(id);

        return R.ok().put("teacher", teacher);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @CacheEvict(value = {"Teacher"}, allEntries = true)
    //@RequiresPermissions("teaching:teacher:save")
    public R save(@Validated({AddGroup.class}) @RequestBody TeacherEntity teacher) {
        teacherService.save(teacher);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @CacheEvict(value = {"Teacher"}, allEntries = true)
    //@RequiresPermissions("teaching:teacher:update")
    public R update(@Validated({UpdateGroup.class}) @RequestBody TeacherEntity teacher) {
        teacherService.updateById(teacher);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @CacheEvict(value = {"Teacher"}, allEntries = true)
    //@RequiresPermissions("teaching:teacher:delete")
    public R delete(@RequestBody Long[] ids) {
        teacherService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
