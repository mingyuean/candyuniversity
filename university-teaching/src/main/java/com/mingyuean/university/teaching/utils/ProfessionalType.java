package com.mingyuean.university.teaching.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author MingYueAn
 * @Description:
 * @Date: 2022/6/2 0:31
 * @Version: 1.0
 */
@Getter
@AllArgsConstructor
public enum ProfessionalType {
    //    Science, engineering, literature, law, agriculture, medicine, philosophy, management, education, history, economics, art
    //    理学、    工学、        文学、       法学、 农学、        医学、     哲学、       管理学、      教育学、     历史学、   经济学、   艺术学
    SCI("a", "理学"),
    ENG("b", "工学"),
    LIT("c", "文学"),
    LAW("d", "法学"),
    AGR("e", "农学"),
    MED("f", "医学"),
    PHI("g", "哲学"),
    MAN("h", "管理学"),
    EDU("i", "教育学"),
    HIS("j", "历史学"),
    ECO("k", "经济学"),
    ART("l", "艺术学");

    private final String typeCode;
    private final String typeName;
}
