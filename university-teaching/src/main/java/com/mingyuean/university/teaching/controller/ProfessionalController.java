package com.mingyuean.university.teaching.controller;

import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.R;
import com.mingyuean.common.validator.group.AddGroup;
import com.mingyuean.common.validator.group.UpdateGroup;
import com.mingyuean.university.teaching.entity.ProfessionalEntity;
import com.mingyuean.university.teaching.service.ProfessionalService;
import com.mingyuean.university.teaching.utils.ProfessionalType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 专业
 *
 * @author MingYueAn
 * @Description: 专业
 * @Date: 2022/5/29 15:52
 * @Version: 1.0
 */
@RestController
@RequestMapping("teaching/professional")
public class ProfessionalController {
    @Autowired
    private ProfessionalService professionalService;

    /**
     * 查询所有专业类型
     */
    @RequestMapping("/findType")
    //@RequiresPermissions("teaching:classes:list")
    public R findType() {
        final List<ProfessionalType> list = Arrays.asList(ProfessionalType.values());
        List<Map<String, java.io.Serializable>> resMap = new ArrayList<>();
        for(int i=0;i<list.size();i++) {
            String name= list.get(i).getTypeName();
            String code= list.get(i).getTypeCode();
            Map<String, java.io.Serializable> map = new HashMap<String, java.io.Serializable>();
            map.put("id",i);
            map.put("code",code);
            map.put("name",name);
            map.put("value",code);
            map.put("text",name);
            resMap.add(map);
        }
        return R.ok().put("professionalTypeList", resMap);
    }

    /**
     * 查询所有专业
     */
    @RequestMapping("/findAll")
    @Cacheable(value = {"Professional"}, key = "#root.methodName", sync = true)
    //@RequiresPermissions("teaching:classes:list")
    public R findAll() {
        final List<ProfessionalEntity> list = professionalService.list();

        return R.ok().put("professional", list);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("teaching:teacher:list")
    public R list(@RequestParam Map<String, Object> params) {
        params.remove("t");
        PageUtils page = professionalService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("teaching:teacher:info")
    public R info(@PathVariable("id") Long id) {
        ProfessionalEntity teacher = professionalService.getById(id);

        return R.ok().put("professional", teacher);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @CacheEvict(value = {"Professional"}, allEntries = true)
    //@RequiresPermissions("teaching:teacher:save")
    public R save(@Validated({AddGroup.class}) @RequestBody ProfessionalEntity professional) {
        professionalService.save(professional);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @CacheEvict(value = {"Professional"}, allEntries = true)
    //@RequiresPermissions("teaching:teacher:update")
    public R update(@Validated({UpdateGroup.class}) @RequestBody ProfessionalEntity professional) {
        professionalService.updateById(professional);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @CacheEvict(value = {"Professional"}, allEntries = true)
    //@RequiresPermissions("teaching:teacher:delete")
    public R delete(@RequestBody Long[] ids) {
        professionalService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
