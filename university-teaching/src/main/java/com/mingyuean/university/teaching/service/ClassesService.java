package com.mingyuean.university.teaching.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.university.teaching.entity.ClassesEntity;

import java.util.List;
import java.util.Map;

/**
 * 班级
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 17:48:54
 */
public interface ClassesService extends IService<ClassesEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<ClassesEntity> init(List<ClassesEntity> selectList);
}

