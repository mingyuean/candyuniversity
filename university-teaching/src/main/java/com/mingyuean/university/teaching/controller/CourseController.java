package com.mingyuean.university.teaching.controller;

import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.R;
import com.mingyuean.common.validator.group.AddGroup;
import com.mingyuean.common.validator.group.UpdateGroup;
import com.mingyuean.university.teaching.entity.CourseEntity;
import com.mingyuean.university.teaching.service.ClassesCourseTeacherService;
import com.mingyuean.university.teaching.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 课程
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 17:48:54
 */
@RestController
@RequestMapping("teaching/course")
public class CourseController {
    @Autowired
    private CourseService courseService;
    @Autowired
    private ClassesCourseTeacherService classesCourseTeacherService;

    /**
     * 查询所有课程
     */
    @RequestMapping("/findAll")
    @Cacheable(value = {"Course"},key =  "#root.methodName",sync = true)
    //@RequiresPermissions("teaching:classes:list")
    public R findAll() {
        final List<CourseEntity> list = courseService.list();

        return R.ok().put("course", list);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("teaching:course:list")
    public R list(@RequestParam Map<String, Object> params) {
        params.remove("t");
        PageUtils page = courseService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("teaching:course:info")
    public R info(@PathVariable("id") Long id) {
        CourseEntity course = courseService.getById(id);

        return R.ok().put("course", course);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @CacheEvict(value = {"Course"}, allEntries = true)
    //@RequiresPermissions("teaching:course:save")
    public R save(@Validated({AddGroup.class}) @RequestBody CourseEntity course) {
        courseService.save(course);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @CacheEvict(value = {"Course"}, allEntries = true)
    //@RequiresPermissions("teaching:course:update")
    public R update(@Validated({UpdateGroup.class}) @RequestBody CourseEntity course) {
        courseService.updateById(course);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @CacheEvict(value = {"Course"}, allEntries = true)
    //@RequiresPermissions("teaching:course:delete")
    public R delete(@RequestBody Long[] ids) {
        courseService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
