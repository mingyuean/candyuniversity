package com.mingyuean.university.teaching;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.mingyuean.university.teaching.dao.*;
import com.mingyuean.university.teaching.entity.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.*;

/**
 * @author MingYueAn
 * @Description: 排课测试
 * @Date: 2022/5/21 9:15
 * @Version: 1.0
 */
@Slf4j
@SpringBootTest
public class CurriculumTest {

    @Autowired
    private ClassesDao classesDao;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private TeacherDao teacherDao;
    @Autowired
    private ClassroomDao classroomDao;
    @Autowired
    private ClassesCourseTeacherDao classesCourseTeacherDao;
    //处理占用信息
    Set<Map> hashSet = new HashSet<>();
    final int week = 5;
    final int list_size = 25;

    public List<ClassesCourseTeacherEntity> init(List<ClassesCourseTeacherEntity> list) {
        //id数组
        List<Long> classesIdList = new ArrayList<>();
        List<Long> courseIdList = new ArrayList<>();
        List<Long> teacherIdList = new ArrayList<>();
        List<Long> classroomIdList = new ArrayList<>();
        //添加id
        list.forEach(item -> {
            classesIdList.add(item.getClassesId());
            courseIdList.add(item.getCourseId());
            teacherIdList.add(item.getTeacherId());
            classroomIdList.add(item.getClassroomId());
        });
        //根据id生成的列表
        final List<ClassesEntity> classesEntityList = classesDao.selectBatchIds(classesIdList);
        final List<CourseEntity> courseEntityList = courseDao.selectBatchIds(courseIdList);
        final List<TeacherEntity> teacherEntityList = teacherDao.selectBatchIds(teacherIdList);
        final List<ClassroomEntity> classroomEntityList = classroomDao.selectBatchIds(classroomIdList);
        //设置实体
        for (ClassesCourseTeacherEntity classesCourseTeacherEntity : list) {
            classesEntityList.forEach(item -> {
                if (item.getId().equals(classesCourseTeacherEntity.getClassesId())) {
                    classesCourseTeacherEntity.setClassesEntity(item);
                }
            });
            courseEntityList.forEach(item -> {
                if (item.getId().equals(classesCourseTeacherEntity.getCourseId())) {
                    classesCourseTeacherEntity.setCourseEntity(item);
                }
            });
//            teacherEntityList.forEach(item -> {
//                if (item.getId().equals(classesCourseTeacherEntity.getTeacherId())) {
//                    classesCourseTeacherEntity.setTeacherEntity(item);
//                }
//            });
//            classroomEntityList.forEach(item -> {
//                if (item.getId().equals(classesCourseTeacherEntity.getClassroomId())) {
//                    classesCourseTeacherEntity.setClassroomEntity(item);
//                }
//            });
        }
        list.forEach(System.out::println);
        return list;
    }

    /**
     * 课程表测试
     */
    @Test
    public void findAllByClassesOrTeacher() {
        QueryWrapper<ClassesCourseTeacherEntity> queryWrapper = new QueryWrapper<>();
        //获取key
        String classesId = "1";
        String teacherId = "";
        //查询
        if (StringUtils.isNotEmpty(classesId)) {
            queryWrapper.eq("classes_id", classesId);
        } else if (StringUtils.isNotEmpty(teacherId)) {
            queryWrapper.eq("teacher_id", teacherId);
        }

        //初始化课表
        List<Curriculum> curriculumList = new ArrayList<>();
        for (int i = 1; i <= week; i++) {
            final Curriculum curriculum = new Curriculum();
            curriculum.setCourse("第" + i + "节");
            curriculumList.add(curriculum);
        }
        List<ClassesCourseTeacherEntity> list = init(classesCourseTeacherDao.selectList(queryWrapper));
        //判断是否存在分课记录
        if (list.size() == 0) {
            list = null;
        }
        Assert.notNull(list, "请先添加分课记录");
        //分课记录遍历
        for (ClassesCourseTeacherEntity classesCourseTeacherEntity : list) {
            //判断是否进行了排课
            Assert.notNull(classesCourseTeacherEntity.getWeekNumber(), "请先进行排课");
            //获取时间的char数组
            char[] weekNumber = classesCourseTeacherEntity.getWeekNumber().toCharArray();
            //数据解析
            analysisWeekNumber(weekNumber, curriculumList, classesCourseTeacherEntity);
        }
        //课表
        System.out.println("curriculumList = " + curriculumList);
        curriculumList.forEach(System.out::println);
    }

    /**
     * 数据解析
     *
     * @param weekNumber     时间 字符数组
     * @param curriculumList 课程表
     * @param courseName     课程名
     */
    private void analysisWeekNumber(char[] weekNumber, List<Curriculum> curriculumList, ClassesCourseTeacherEntity courseName) {
        System.out.println("weekNumber = " + weekNumber[0]);
        System.out.println("第？节课，n = " + Integer.parseInt(String.valueOf(weekNumber[1])));
        System.out.println("courseName = " + courseName);
        String time="";
        //数据处理
        switch (weekNumber[0]) {
            case 'A':
                curriculumList.get(Integer.parseInt(String.valueOf(weekNumber[1])) - 1).setOne(courseName);
                time="星期一 "+"第"+weekNumber[1]+"节课";
                break;
            case 'B':
                curriculumList.get(Integer.parseInt(String.valueOf(weekNumber[1])) - 1).setTwo(courseName);
                time="星期二 "+"第"+weekNumber[1]+"节课";
                break;
            case 'C':
                curriculumList.get(Integer.parseInt(String.valueOf(weekNumber[1])) - 1).setThree(courseName);
                time="星期三 "+"第"+weekNumber[1]+"节课";
                break;
            case 'D':
                curriculumList.get(Integer.parseInt(String.valueOf(weekNumber[1])) - 1).setFour(courseName);
                time="星期四 "+"第"+weekNumber[1]+"节课";
                break;
            case 'E':
                curriculumList.get(Integer.parseInt(String.valueOf(weekNumber[1])) - 1).setFive(courseName);
                time="星期五 "+"第"+weekNumber[1]+"节课";
                break;
        }
        log.info(time);
    }

    /**
     * 测试CurriculumServiceImpl的timeTable
     * <p>
     * 排课
     * <p>
     * 初次加载数据
     */
    @Test
    public void timeTable() {
        //条件修改
        final UpdateWrapper<ClassesCourseTeacherEntity> updateWrapper = new UpdateWrapper<>();
        //全部置空
        updateWrapper.set("week_number", null);
        updateWrapper.set("classroom_id", null);
        //修改表
        classesCourseTeacherDao.update(null, updateWrapper);

        //查询所有分课记录
        List<ClassesCourseTeacherEntity> list = init(classesCourseTeacherDao.selectList(new QueryWrapper<>()));
        if (list.size() == 0) {
            list = null;
        }
        Assert.notNull(list, "请先添加分课记录");
        //查询所有教室记录
        List<ClassroomEntity> classroomList = classroomDao.selectList(new QueryWrapper<>());
        if (classroomList.size() == 0) {
            classroomList = null;
        }
        Assert.notNull(classroomList, "请先添加教室");
        //循环遍历
        for (ClassesCourseTeacherEntity classesCourseTeacherEntity : list) {
            log.warn("当前分课id：{}", classesCourseTeacherEntity.getId());
            //可用教室列表
            List<ClassroomEntity> okClassroomList = okClassroomList(classroomList, classesCourseTeacherEntity.getClassesEntity().getClassesNumber());
            log.info("可用教室列表：{}", okClassroomList);
            //okClassroomList=null，可用教室列表为空
            Assert.notNull(okClassroomList, "教室空间不足，不符合班级人数要求");
            //时间冲突，分配教室，分配时间
            Map<String, String> map = byWeekNumberAndClassroomId(okClassroomList, new HashSet<>());
            //map=null，教室不够
            Assert.notNull(map, "教室数量不足，请增加教室");

            System.out.println("时间：" + map.get("week_number"));
            System.out.println("教室：" + map.get("classroom_id"));

            classesCourseTeacherEntity.setWeekNumber(map.get("week_number"));
            classesCourseTeacherEntity.setClassroomId(Long.valueOf(map.get("classroom_id")));
            //保存修改后的记录
//            classesCourseTeacherDao.updateById(classesCourseTeacherEntity);
        }
        list.forEach(System.out::println);
    }


    /**
     * 排课，分配随机时间，分配随机教室，解决教室时间冲突
     *
     * @param okClassroomList 可用教室列表
     * @param errorSet        冲突遍历
     * @return map=时间+教室
     */
    private Map<String, String> byWeekNumberAndClassroomId(List<ClassroomEntity> okClassroomList, HashSet<Object> errorSet) {
        log.info("冲突遍历{}", errorSet);
        Map<String, String> map = new HashMap<>(2);
        //随机时间
        map.put("week_number", randomWeekNumber());
        log.info("随机时间 = {}", map.get("week_number"));
        //随机可用教室
        map.put("classroom_id", randomClassroomEntity(okClassroomList).toString());
        log.info("随机教室 = {}", map.get("classroom_id"));
        if (hashSet.add(map)) {
            log.info("无重复");
            return map;
        } else if (errorSet.size() == okClassroomList.size() * list_size) {
            log.error("时间+教室，完全遍历");
            return null;
        } else {
            log.error("重复");
            map.replace("week_number", randomWeekNumber());
            map.replace("classroom_id", randomClassroomEntity(okClassroomList).toString());
            errorSet.add(map);
            return byWeekNumberAndClassroomId(okClassroomList, errorSet);
        }
    }

    /**
     * 可用教室
     *
     * @param classesNumber
     * @return
     */
    private List<ClassroomEntity> okClassroomList(List<ClassroomEntity> classroomList, Integer classesNumber) {
        List<ClassroomEntity> okClassroomList = new ArrayList<>();
        for (ClassroomEntity classroomEntity : classroomList) {
            if (classroomEntity.getClassroomSize() >= classesNumber) {
                //符合 随机分配的教室大小>=分课记录中班级的人数
                log.info("符合要求，教室可用：{}", classroomEntity.getClassroomName());
                okClassroomList.add(classroomEntity);
            }
        }
        if (okClassroomList.size() == 0) {
            return null;
        } else {
            return okClassroomList;
        }
    }


    /**
     * 随机可用教室
     *
     * @param classroomList
     * @return
     */
    private Long randomClassroomEntity(List<ClassroomEntity> classroomList) {
        //随机下标
        int random1 = new Random().nextInt(classroomList.size()) + 1;
        //随机教室
        return classroomList.get(random1 - 1).getId();
    }


    /**
     * 随机时间
     *
     * @return
     */
    private String randomWeekNumber() {
        String weekNumber = "";
        /**
         * 星期 ABCDE
         * 课 01、02、03、04、05、06、07、08、09、10
         * 1:01\02
         * 2:03\04
         * 3:05\06
         * 4:07\08
         * 5:09\10
         */
        int x = new Random().nextInt(5) + 1;
//        System.out.println("星期：x=" + x);
        switch (x) {
            case 1:
                weekNumber = "A";
                break;
            case 2:
                weekNumber = "B";
                break;
            case 3:
                weekNumber = "C";
                break;
            case 4:
                weekNumber = "D";
                break;
            case 5:
                weekNumber = "E";
                break;
        }
//        System.out.println("1 weekNumber = " + weekNumber);
        int y = new Random().nextInt(5) + 1;
//        System.out.println("课：y=" + y);
        switch (y) {
            case 1:
                weekNumber = weekNumber + "1:01\\02";
                break;
            case 2:
                weekNumber = weekNumber + "2:03\\04";
                break;
            case 3:
                weekNumber = weekNumber + "3:05\\06";
                break;
            case 4:
                weekNumber = weekNumber + "4:07\\08";
                break;
            case 5:
                weekNumber = weekNumber + "5:09\\10";
                break;
        }
//        System.out.println("2 weekNumber = " + weekNumber);
        return weekNumber;
    }
}
