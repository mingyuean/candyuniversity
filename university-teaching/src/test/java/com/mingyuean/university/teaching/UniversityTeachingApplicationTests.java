package com.mingyuean.university.teaching;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.mingyuean.university.teaching.dao.ClassesCourseTeacherDao;
import com.mingyuean.university.teaching.dao.ClassesDao;
import com.mingyuean.university.teaching.dao.CourseDao;
import com.mingyuean.university.teaching.dao.TeacherDao;
import com.mingyuean.university.teaching.entity.ClassesCourseTeacherEntity;
import com.mingyuean.university.teaching.entity.ClassroomEntity;
import com.mingyuean.university.teaching.entity.CourseEntity;
import com.mingyuean.university.teaching.service.ClassroomService;
import com.mingyuean.university.teaching.utils.ProfessionalType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@SpringBootTest
class UniversityTeachingApplicationTests {
    @Autowired
    private ClassesDao classesDao;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private TeacherDao teacherDao;
    @Autowired
    private ClassesCourseTeacherDao classesCourseTeacherDao;

//    @Autowired
//    ClassesCourseTeacherService service;
//    @Autowired
//    ClassesService service;
//    @Autowired
//    CourseService service;
//    @Autowired
//    TeacherService service;
    @Autowired
    ClassroomService service;

    /**
     * 多条操作
     */
    @Test
    void addClasses() {
        List<ClassroomEntity> entityList = new ArrayList<>(11);
        for (int i = 3; i < 11; i++) {
            ClassroomEntity entity = new ClassroomEntity();
            entity.setClassroomName("实验室"+i);
            entity.setClassroomSize(50);
            entity.setClassroomType("1");
            entityList.add(entity);
        }
        service.saveBatch(entityList);
    }

    @Test
    public void name() {
        final ProfessionalType[] values = ProfessionalType.values();
        log.error("{}",values[8]);
    }

    /**
     * 测试ClassesCourseTeacherService的updateCourseName
     */
    @Test
    public void updateCourseName() {
        CourseEntity course = new CourseEntity();
        course.setId(2L);
        course.setCourseName("xiugai");
        //条件修改
        UpdateWrapper<ClassesCourseTeacherEntity> updateWrapper = new UpdateWrapper<>();
        //通过修改课程的id查询分课表中存在课程id的记录
        updateWrapper.eq("course_id", course.getId());
        //只修改表中课程名
        updateWrapper.set("course_name", course.getCourseName());
        //修改表
        classesCourseTeacherDao.update(null, updateWrapper);
    }

    /**
     * 测试ClassesCourseTeacherService的listByClassesId
     * <p>
     * 通过班级id查询查询该班级的分课信息
     */
    @Test
    public void listByClassesId() {
        QueryWrapper<ClassesCourseTeacherEntity> queryWrapper = new QueryWrapper<>();
        //获取key
        String classesKey = "1";
        //查询classes
        if (StringUtils.isNotEmpty(classesKey)) {
            queryWrapper.eq("classes_id", classesKey);
            System.out.println("classesKey = " + classesKey);
        }
        final List<ClassesCourseTeacherEntity> list = classesCourseTeacherDao.selectList(queryWrapper);
        list.forEach(System.out::println);

        for (ClassesCourseTeacherEntity classesCourseTeacherEntity : list) {
            classesCourseTeacherEntity.setClassesEntity(classesDao.selectById(classesCourseTeacherEntity.getClassesId()));
            classesCourseTeacherEntity.setCourseEntity(courseDao.selectById(classesCourseTeacherEntity.getCourseId()));
            classesCourseTeacherEntity.setTeacherEntity(teacherDao.selectById(classesCourseTeacherEntity.getTeacherId()));
        }
        list.forEach(System.out::println);
    }

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void redisTest() {
        //存
        redisTemplate.opsForValue().set("121", "122");
        //取
        Object o = redisTemplate.opsForValue().get("121");
        System.out.println(o);
    }
}