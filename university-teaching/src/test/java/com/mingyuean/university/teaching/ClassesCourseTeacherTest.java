package com.mingyuean.university.teaching;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mingyuean.university.teaching.dao.*;
import com.mingyuean.university.teaching.entity.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author MingYueAn
 * @Description: 测试
 * @Date: 2022/5/23 21:44
 * @Version: 1.0
 */
@Slf4j
@SpringBootTest
public class ClassesCourseTeacherTest {
    @Autowired
    private ClassesDao classesDao;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private TeacherDao teacherDao;
    @Autowired
    private ClassroomDao classroomDao;
    @Autowired
    private ClassesCourseTeacherDao classesCourseTeacherDao;

    @Test
    public void name() {
        QueryWrapper<ClassesCourseTeacherEntity> queryWrapper = new QueryWrapper<>();
        //所有分课记录
        List<ClassesCourseTeacherEntity> list = classesCourseTeacherDao.selectList(queryWrapper);
        //id数组
        List<Long> classesIdList = new ArrayList<>();
        List<Long> courseIdList = new ArrayList<>();
        List<Long> teacherIdList = new ArrayList<>();
        List<Long> classroomIdList = new ArrayList<>();
        //添加id
        list.forEach(item -> {
            classesIdList.add(item.getClassesId());
            courseIdList.add(item.getCourseId());
            teacherIdList.add(item.getTeacherId());
            classroomIdList.add(item.getClassroomId());
        });
        //根据id生成的列表
        final List<ClassesEntity> classesEntityList = classesDao.selectBatchIds(classesIdList);
        final List<CourseEntity> courseEntityList = courseDao.selectBatchIds(courseIdList);
        final List<TeacherEntity> teacherEntityList = teacherDao.selectBatchIds(teacherIdList);
        final List<ClassroomEntity> classroomEntityList = classroomDao.selectBatchIds(classroomIdList);
        //设置实体
        for (ClassesCourseTeacherEntity classesCourseTeacherEntity : list) {
            classesEntityList.forEach(item -> {
                if (item.getId().equals(classesCourseTeacherEntity.getClassesId())) {
                    classesCourseTeacherEntity.setClassesEntity(item);
                }
            });
            courseEntityList.forEach(item -> {
                if (item.getId().equals(classesCourseTeacherEntity.getCourseId())) {
                    classesCourseTeacherEntity.setCourseEntity(item);
                }
            });
            teacherEntityList.forEach(item -> {
                if (item.getId().equals(classesCourseTeacherEntity.getTeacherId())) {
                    classesCourseTeacherEntity.setTeacherEntity(item);
                }
            });
            classroomEntityList.forEach(item -> {
                if (item.getId().equals(classesCourseTeacherEntity.getClassroomId())) {
                    classesCourseTeacherEntity.setClassroomEntity(item);
                }
            });
        }
        list.forEach(System.out::println);
    }
}
