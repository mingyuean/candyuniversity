package com.mingyuean.university.teaching;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mingyuean.university.teaching.dao.ClassesDao;
import com.mingyuean.university.teaching.dao.SemesterDao;
import com.mingyuean.university.teaching.entity.ClassesEntity;
import com.mingyuean.university.teaching.entity.SemesterEntity;
import com.mingyuean.university.teaching.service.ClassesService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author MingYueAn
 * @Description:
 * @Date: 2022/5/25 17:17
 * @Version: 1.0
 */
@SpringBootTest
@Slf4j
public class DateTest {
    @Autowired
    SemesterDao semesterDao;

    @Test
    public void name() {
        final SemesterEntity semesterEntity = semesterDao.selectById(1L);
        log.info("{}", semesterEntity);
        //当前时间
        LocalDate today = LocalDate.now();
        LocalDate monday = today.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        LocalDate sunday = today.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));
        System.out.println("本周开始时间" + monday);
        System.out.println("本周结束时间" + sunday);

        //获取开始时间的周一
        LocalDate start1 = semesterEntity.getSemesterStartDate().with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        LocalDate end1 = semesterEntity.getSemesterEndDate().with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));

        System.out.println("start1 = " + start1);
        System.out.println("end1 = " + end1);

        long days = end1.toEpochDay() - start1.toEpochDay();//得出相差天数
        System.out.println("相差天数" + days);
        double week = (days / 7) + 1;
        System.out.println("week = " + week);
    }

    @Autowired
    private ClassesDao classesDao;
    @Autowired
    private ClassesService classesService;

    @Test
    public void n1() {
        int[] arr = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        final List<ClassesEntity> classesEntityList = classesService.init(classesDao.selectList(new QueryWrapper<>()));
        classesEntityList.forEach(item -> {
            switch (item.getProfessionalEntity().getProfessionalType()) {
                case "a":
                    arr[0]++;
                    break;
                case "b":
                    arr[1]++;
                    break;
                case "c":
                    arr[2]++;
                    break;
                case "d":
                    arr[3]++;
                    break;
                case "e":
                    arr[4]++;
                    break;
                case "f":
                    arr[5]++;
                    break;
                case "g":
                    arr[6]++;
                    break;
                case "h":
                    arr[7]++;
                    break;
                case "i":
                    arr[8]++;
                    break;
                case "j":
                    arr[8]++;
                    break;
                case "k":
                    arr[10]++;
                    break;
                case "l":
                    arr[11]++;
                    break;
                default:
                    break;
            }
        });
        // int[] 转 List<Integer>
        // Arrays.stream将int[]转化为IntStream
        // boxed()装箱，将IntStream转化为Stream<Integer>
        // collect将Stream<Integer>转化为List<Integer>
        List<Integer> list1 = Arrays.stream(arr).boxed().collect(Collectors.toList());
        log.error("{}", list1);
    }
}
