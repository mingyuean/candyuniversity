package com.mingyuean.university.teaching;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mingyuean.university.teaching.dao.SemesterDao;
import com.mingyuean.university.teaching.entity.SemesterEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.List;

/**
 * @author MingYueAn
 * @Description: 测试
 * @Date: 2022/5/24 18:33
 * @Version: 1.0
 */
@SpringBootTest
@Slf4j
public class SemesterTest {
    @Autowired
    private SemesterDao semesterDao;
    @Test
    public void name(){

        SemesterEntity semesterEntity = new SemesterEntity();
        semesterEntity.setSemesterStartDate(LocalDate.of(2015, 12, 31));
        semesterDao.insert(semesterEntity);

        final List<SemesterEntity>semesterEntityList = semesterDao.selectList(new QueryWrapper<>());
        semesterEntityList.forEach(System.out::println);
        final LocalDate semesterStartDate = semesterEntityList.get(0).getSemesterStartDate();
        System.out.println("semesterStartDate = " + semesterStartDate);
    }
}
