package com.mingyuean.university.nomenclature;

import com.mingyuean.university.nomenclature.entity.ClassificationEntity;
import com.mingyuean.university.nomenclature.service.ClassificationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class UniversityNomenclatureApplicationTests {
    @Autowired
    private ClassificationService classificationService;

    /**
     * 多条操作
     */
    @Test
    void addClasses() {
        List<ClassificationEntity> entityList = new ArrayList<>(20);
        for (int i = 1; i < 20; i++) {
            ClassificationEntity classificationEntity = new ClassificationEntity();
            classificationEntity.setClassificationName("分类");
            entityList.add(classificationEntity);
        }
        System.out.println("classificationService.saveBatch(entityList) = " + classificationService.saveBatch(entityList));
    }

}
