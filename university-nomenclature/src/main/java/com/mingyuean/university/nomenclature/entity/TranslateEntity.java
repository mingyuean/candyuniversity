package com.mingyuean.university.nomenclature.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 中英翻译
 * 
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-17 17:42:04
 */
@Data
@TableName("tee_translate")
public class TranslateEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 序号
	 */
	@TableId
	private Long id;
	/**
	 * 中文
	 */
	private String translateChinese;
	/**
	 * 英文
	 */
	private String translateEnglish;

}
