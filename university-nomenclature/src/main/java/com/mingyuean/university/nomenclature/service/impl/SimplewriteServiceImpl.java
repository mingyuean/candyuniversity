package com.mingyuean.university.nomenclature.service.impl;

import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.Query;
import com.mingyuean.university.nomenclature.entity.ClassificationEntity;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


import com.mingyuean.university.nomenclature.dao.SimplewriteDao;
import com.mingyuean.university.nomenclature.entity.SimplewriteEntity;
import com.mingyuean.university.nomenclature.service.SimplewriteService;


@Service("simplewriteService")
public class SimplewriteServiceImpl extends ServiceImpl<SimplewriteDao, SimplewriteEntity> implements SimplewriteService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<SimplewriteEntity> queryWrapper = new QueryWrapper<>();
        //获取key
        String key = (String) params.get("key");
        if (StringUtils.isNotEmpty(key)) {
            //模糊查询
            queryWrapper.and((item) -> {
                item.eq("id", key);
            }).or((item) -> {
                item.like("simplewrite_name", key);
            });
        }
        IPage<SimplewriteEntity> page = this.page(
                new Query<SimplewriteEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

}