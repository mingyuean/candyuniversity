package com.mingyuean.university.nomenclature.service;

import com.mingyuean.university.nomenclature.utils.TransResult;

import java.util.Map;

/**
 * 中英翻译
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-17 17:42:04
 */
public interface TranslateService {

    /**
     * 中译英
     * @param params
     * @return
     */
    TransResult cntoen(Map<String, Object> params);

    /**
     * 英译中
     * @param params
     * @return
     */
    TransResult entocn(Map<String, Object> params);

}

