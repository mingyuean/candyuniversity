package com.mingyuean.university.nomenclature.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.university.nomenclature.entity.VocabularyEntity;

import java.util.Map;

/**
 * 词汇管理
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-17 17:42:04
 */
public interface VocabularyService extends IService<VocabularyEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

