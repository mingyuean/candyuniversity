package com.mingyuean.university.nomenclature.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.mingyuean.university.nomenclature.utils.TransApi;
import com.mingyuean.university.nomenclature.utils.TransClass;
import com.mingyuean.university.nomenclature.utils.TransResult;
import org.springframework.stereotype.Service;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.mingyuean.university.nomenclature.service.TranslateService;


@Service("translateService")
public class TranslateServiceImpl implements TranslateService {
    // 在平台申请的APP_ID 详见 http://api.fanyi.baidu.com/api/trans/product/desktop?req=developer
    private static final String APP_ID = "20220517001219398";
    private static final String SECURITY_KEY = "u42NwuZbyatOpupRW7GQ";

    /**
     * 中译英
     *
     * @param params
     * @return
     */
    @Override
    public TransResult cntoen(Map<String, Object> params) {
        TransApi api = new TransApi(APP_ID, SECURITY_KEY);
        //获取key
        String key = (String) params.get("key");
        if (StringUtils.isNotEmpty(key)) {
            System.out.println("key = " + key);
            final String transString = api.getTransResult(key, "zh", "en");
            //翻译结果字符串
            System.out.println("transResult = " + transString);
            //json转对象
            ObjectMapper mapper = new ObjectMapper();
            TransClass transClass = null;
            try {
                transClass = mapper.readValue(transString, TransClass.class);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            System.out.println("结果 = " + transClass.getTransResult().get(0).getDst());

            return transClass.getTransResult().get(0);
        }
        return null;
    }

    /**
     * 英译中
     *
     * @param params
     * @return
     */
    @Override
    public TransResult entocn(Map<String, Object> params) {
        TransApi api = new TransApi(APP_ID, SECURITY_KEY);
        //获取key
        String key = (String) params.get("key");
        if (StringUtils.isNotEmpty(key)) {
            System.out.println("key = " + key);
            final String transString = api.getTransResult(key, "en", "zh");
            //翻译结果字符串
            System.out.println("transResult = " + transString);
            //json转对象
            ObjectMapper mapper = new ObjectMapper();
            TransClass transClass = null;
            try {
                transClass = mapper.readValue(transString, TransClass.class);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            System.out.println("结果 = " + transClass.getTransResult().get(0).getDst());

            return transClass.getTransResult().get(0);
        }
        return null;
    }

}