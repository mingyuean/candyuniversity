package com.mingyuean.university.nomenclature.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 词汇管理
 * 
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-17 17:42:04
 */
@Data
@TableName("tee_vocabulary")
public class VocabularyEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 序号
	 */
	@TableId
	private Long id;
	/**
	 * 名字
	 */
	private String vocabularyName;
	/**
	 * 描述
	 */
	private String vocabularyDescribe;
	/**
	 * 分类id
	 */
	private String classificationId;
	/**
	 * 分类名
	 */
	private String classificationName;

}
