package com.mingyuean.university.nomenclature.exception;

import com.mingyuean.common.exception.ResultCode;
import com.mingyuean.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * 统一处理异常
 *
 * //@ResponseBody
 * //@ControllerAdvice(basePackages = "com.mingyuean.university.teaching.controller")
 * @Author: MingYueAn
 * @Description: 统一处理异常
 * @Date: 2022/5/13 17:48
 * @Version: 1.0
 */
@Slf4j
@RestControllerAdvice(basePackages = "com.mingyuean.university.nomenclature.controller")
public class ExceptionControllerAdvice {
    /**
     * 数据校验异常
     *
     * @param e 异常
     * @return 结果集
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public R handleValidException(MethodArgumentNotValidException e) {
        log.error("数据校验异常，异常类型：{}", e.getClass());
        //获取数据校验结果
        final BindingResult bindingResult = e.getBindingResult();
        //封装
        Map<String, String> errorMap = new HashMap<>();
        //遍历FieldError
        bindingResult.getFieldErrors().forEach((item) -> {
            //获取错误提示
            String message = item.getDefaultMessage();
            //获取错误字段名
            String field = item.getField();
            //map封装
            errorMap.put(field, message);
        });
        return R.error(ResultCode.VALID_EXCEPTION.getCode(), ResultCode.VALID_EXCEPTION.getMessage()).put("data", errorMap);
    }

    /**
     * 未知异常
     *
     * @param e 异常
     * @return 结果集
     */
    @ExceptionHandler(value = Throwable.class)
    public R handleException(Throwable e) {
        log.error("未知异常，异常类型：{}", e.getClass());
        log.error("未知异常，异常消息：{}", e.getMessage());
        return R.error(ResultCode.ERROR.getCode(), ResultCode.ERROR.getMessage());
    }
}
