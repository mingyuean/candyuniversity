package com.mingyuean.university.nomenclature.service.impl;

import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.Query;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.mingyuean.university.nomenclature.dao.VocabularyDao;
import com.mingyuean.university.nomenclature.entity.VocabularyEntity;
import com.mingyuean.university.nomenclature.service.VocabularyService;


@Service("vocabularyService")
public class VocabularyServiceImpl extends ServiceImpl<VocabularyDao, VocabularyEntity> implements VocabularyService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<VocabularyEntity> queryWrapper = new QueryWrapper<>();
        //获取key
        String key = (String) params.get("key");
        if (StringUtils.isNotEmpty(key)) {
            //模糊查询
            queryWrapper.and((item) -> {
                item.eq("id", key);
            }).or((item) -> {
                item.like("vocabulary_name", key);
            });
        }
        IPage<VocabularyEntity> page = this.page(
                new Query<VocabularyEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

}