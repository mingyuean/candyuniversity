package com.mingyuean.university.nomenclature.controller;

import java.util.Arrays;
import java.util.Map;

import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mingyuean.university.nomenclature.entity.ClassificationEntity;
import com.mingyuean.university.nomenclature.service.ClassificationService;




/**
 * 分类管理
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-17 17:42:04
 */
@RestController
@RequestMapping("nomenclature/classification")
public class ClassificationController {
    @Autowired
    private ClassificationService classificationService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("nomenclature:classification:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = classificationService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("nomenclature:classification:info")
    public R info(@PathVariable("id") Long id){
		ClassificationEntity classification = classificationService.getById(id);

        return R.ok().put("classification", classification);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("nomenclature:classification:save")
    public R save(@RequestBody ClassificationEntity classification){
		classificationService.save(classification);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("nomenclature:classification:update")
    public R update(@RequestBody ClassificationEntity classification){
		classificationService.updateById(classification);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("nomenclature:classification:delete")
    public R delete(@RequestBody Long[] ids){
		classificationService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
