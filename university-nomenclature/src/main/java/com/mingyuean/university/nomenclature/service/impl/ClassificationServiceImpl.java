package com.mingyuean.university.nomenclature.service.impl;

import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.Query;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


import com.mingyuean.university.nomenclature.dao.ClassificationDao;
import com.mingyuean.university.nomenclature.entity.ClassificationEntity;
import com.mingyuean.university.nomenclature.service.ClassificationService;


@Service("classificationService")
public class ClassificationServiceImpl extends ServiceImpl<ClassificationDao, ClassificationEntity> implements ClassificationService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<ClassificationEntity> queryWrapper = new QueryWrapper<>();
        //获取key
        String key = (String) params.get("key");
        if (StringUtils.isNotEmpty(key)) {
            //模糊查询
            queryWrapper.and((item) -> {
                item.eq("id", key);
            }).or((item) -> {
                item.like("classification_name", key);
            });
        }
        IPage<ClassificationEntity> page = this.page(
                new Query<ClassificationEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

}