package com.mingyuean.university.nomenclature.dao;

import com.mingyuean.university.nomenclature.entity.TranslateEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 中英翻译
 * 
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-17 17:42:04
 */
@Mapper
public interface TranslateDao extends BaseMapper<TranslateEntity> {
	
}
