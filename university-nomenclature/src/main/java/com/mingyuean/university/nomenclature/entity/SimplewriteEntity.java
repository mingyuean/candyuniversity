package com.mingyuean.university.nomenclature.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 缩写管理
 * 
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-17 20:56:25
 */
@Data
@TableName("tee_simplewrite")
public class SimplewriteEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 序号
	 */
	@TableId
	private Long id;
	/**
	 * 缩写名
	 */
	private String simplewriteName;
	/**
	 * 描述
	 */
	private String simplewriteDescribe;

}
