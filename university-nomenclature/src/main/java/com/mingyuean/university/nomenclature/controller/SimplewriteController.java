package com.mingyuean.university.nomenclature.controller;

import java.util.Arrays;
import java.util.Map;

import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mingyuean.university.nomenclature.entity.SimplewriteEntity;
import com.mingyuean.university.nomenclature.service.SimplewriteService;


/**
 * 缩写管理
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-17 20:56:25
 */
@RestController
@RequestMapping("nomenclature/simplewrite")
public class SimplewriteController {
    @Autowired
    private SimplewriteService simplewriteService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("nomenclature:simplewrite:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = simplewriteService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("nomenclature:simplewrite:info")
    public R info(@PathVariable("id") Long id) {
        SimplewriteEntity simplewrite = simplewriteService.getById(id);

        return R.ok().put("simplewrite", simplewrite);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("nomenclature:simplewrite:save")
    public R save(@RequestBody SimplewriteEntity simplewrite) {
        simplewriteService.save(simplewrite);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("nomenclature:simplewrite:update")
    public R update(@RequestBody SimplewriteEntity simplewrite) {
        simplewriteService.updateById(simplewrite);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("nomenclature:simplewrite:delete")
    public R delete(@RequestBody Long[] ids) {
        simplewriteService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
