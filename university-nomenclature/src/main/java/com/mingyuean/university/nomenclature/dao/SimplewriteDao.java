package com.mingyuean.university.nomenclature.dao;

import com.mingyuean.university.nomenclature.entity.SimplewriteEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 缩写管理
 * 
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-17 20:56:25
 */
@Mapper
public interface SimplewriteDao extends BaseMapper<SimplewriteEntity> {
	
}
