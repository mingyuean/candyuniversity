package com.mingyuean.university.nomenclature.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 分类管理
 * 
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-17 17:42:04
 */
@Data
@TableName("tee_classification")
public class ClassificationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 序号
	 */
	@TableId
	private Long id;
	/**
	 * 名字
	 */
	private String classificationName;

}
