package com.mingyuean.university.nomenclature.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.university.nomenclature.entity.SimplewriteEntity;

import java.util.Map;

/**
 * 缩写管理
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-17 20:56:25
 */
public interface SimplewriteService extends IService<SimplewriteEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

