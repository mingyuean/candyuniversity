package com.mingyuean.university.nomenclature.controller;

import java.util.Arrays;
import java.util.Map;

import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mingyuean.university.nomenclature.entity.VocabularyEntity;
import com.mingyuean.university.nomenclature.service.VocabularyService;




/**
 * 词汇管理
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-17 17:42:04
 */
@RestController
@RequestMapping("nomenclature/vocabulary")
public class VocabularyController {
    @Autowired
    private VocabularyService vocabularyService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("nomenclature:vocabulary:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = vocabularyService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("nomenclature:vocabulary:info")
    public R info(@PathVariable("id") Long id){
		VocabularyEntity vocabulary = vocabularyService.getById(id);

        return R.ok().put("vocabulary", vocabulary);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("nomenclature:vocabulary:save")
    public R save(@RequestBody VocabularyEntity vocabulary){
		vocabularyService.save(vocabulary);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("nomenclature:vocabulary:update")
    public R update(@RequestBody VocabularyEntity vocabulary){
		vocabularyService.updateById(vocabulary);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("nomenclature:vocabulary:delete")
    public R delete(@RequestBody Long[] ids){
		vocabularyService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
