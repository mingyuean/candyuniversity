package com.mingyuean.university.nomenclature.dao;

import com.mingyuean.university.nomenclature.entity.ClassificationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 分类管理
 * 
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-17 17:42:04
 */
@Mapper
public interface ClassificationDao extends BaseMapper<ClassificationEntity> {
	
}
