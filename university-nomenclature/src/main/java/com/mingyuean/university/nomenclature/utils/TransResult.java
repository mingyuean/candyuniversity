package com.mingyuean.university.nomenclature.utils;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author MingYueAn
 * @Description: 翻译返回结果（src、dst）
 * @Date: 2022/5/18 13:53
 * @Version: 1.0
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "src",
        "dst"
})
public class TransResult {

    @JsonProperty("src")
    private String src;
    @JsonProperty("dst")
    private String dst;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("src")
    public String getSrc() {
        return src;
    }

    @JsonProperty("src")
    public void setSrc(String src) {
        this.src = src;
    }

    @JsonProperty("dst")
    public String getDst() {
        return dst;
    }

    @JsonProperty("dst")
    public void setDst(String dst) {
        this.dst = dst;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(TransResult.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("src");
        sb.append('=');
        sb.append(((this.src == null) ? "<null>" : this.src));
        sb.append(',');
        sb.append("dst");
        sb.append('=');
        sb.append(((this.dst == null) ? "<null>" : this.dst));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null) ? "<null>" : this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.additionalProperties == null) ? 0 : this.additionalProperties.hashCode()));
        result = ((result * 31) + ((this.dst == null) ? 0 : this.dst.hashCode()));
        result = ((result * 31) + ((this.src == null) ? 0 : this.src.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof TransResult) == false) {
            return false;
        }
        TransResult rhs = ((TransResult) other);
        return ((((this.additionalProperties == rhs.additionalProperties) || ((this.additionalProperties != null) && this.additionalProperties.equals(rhs.additionalProperties))) && ((this.dst == rhs.dst) || ((this.dst != null) && this.dst.equals(rhs.dst)))) && ((this.src == rhs.src) || ((this.src != null) && this.src.equals(rhs.src))));
    }

}

