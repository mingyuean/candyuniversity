package com.mingyuean.university.nomenclature.controller;

import java.util.Map;

import com.mingyuean.common.utils.R;
import com.mingyuean.university.nomenclature.utils.TransResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mingyuean.university.nomenclature.service.TranslateService;


/**
 * 中英翻译
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-17 17:42:04
 */
@RestController
@RequestMapping("nomenclature/translate")
public class TranslateController {
    @Autowired
    private TranslateService translateService;

    /**
     * 中译英
     */
    @RequestMapping("/cntoen")
    //@RequiresPermissions("nomenclature:translate:list")
    public R cntoen(@RequestParam Map<String, Object> params) {
        final TransResult cntoen = translateService.cntoen(params);

        return R.ok().put("translateResult", cntoen);
    }

    /**
     * 英译中
     */
    @RequestMapping("/entocn")
    //@RequiresPermissions("nomenclature:translate:list")
    public R entocn(@RequestParam Map<String, Object> params) {
        final TransResult entocn = translateService.entocn(params);

        return R.ok().put("translateResult",entocn);
    }
}
