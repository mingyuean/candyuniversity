/*
 Navicat MySQL Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80029
 Source Host           : localhost:3306
 Source Schema         : univ_teaching

 Target Server Type    : MySQL
 Target Server Version : 80029
 File Encoding         : 65001

 Date: 07/06/2022 21:40:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tcg_classes
-- ----------------------------
DROP TABLE IF EXISTS `tcg_classes`;
CREATE TABLE `tcg_classes`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '序号',
  `classes_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名字',
  `classes_number` int NOT NULL DEFAULT 50 COMMENT '人数',
  `classes_week_max` int NOT NULL DEFAULT 8 COMMENT '每周课程上限',
  `professional_id` bigint NOT NULL COMMENT '专业id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '班级' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tcg_classes
-- ----------------------------
INSERT INTO `tcg_classes` VALUES (1, '物联01', 50, 25, 2);
INSERT INTO `tcg_classes` VALUES (2, '物联02', 20, 0, 2);
INSERT INTO `tcg_classes` VALUES (3, '园林01', 50, 25, 5);
INSERT INTO `tcg_classes` VALUES (4, '汉语言文学1', 50, 25, 3);
INSERT INTO `tcg_classes` VALUES (5, '知识产权2', 50, 25, 4);
INSERT INTO `tcg_classes` VALUES (6, '知识产权3', 50, 25, 4);
INSERT INTO `tcg_classes` VALUES (7, '园林4', 50, 25, 5);
INSERT INTO `tcg_classes` VALUES (8, '汉语言文学5', 50, 25, 3);
INSERT INTO `tcg_classes` VALUES (9, '汉语言文学6', 50, 25, 3);
INSERT INTO `tcg_classes` VALUES (10, '汉语言文学7', 50, 25, 3);
INSERT INTO `tcg_classes` VALUES (11, '汉语言文学8', 50, 25, 3);
INSERT INTO `tcg_classes` VALUES (12, '汉语言文学9', 50, 25, 3);
INSERT INTO `tcg_classes` VALUES (13, '汉语言文学10', 50, 25, 3);
INSERT INTO `tcg_classes` VALUES (14, '汉语言文学11', 50, 25, 3);
INSERT INTO `tcg_classes` VALUES (15, '汉语言文学12', 50, 25, 3);
INSERT INTO `tcg_classes` VALUES (16, '汉语言文学13', 50, 25, 3);
INSERT INTO `tcg_classes` VALUES (17, '汉语言文学14', 50, 25, 3);
INSERT INTO `tcg_classes` VALUES (18, '汉语言文学15', 50, 25, 3);
INSERT INTO `tcg_classes` VALUES (19, '汉语言文学16', 50, 25, 3);
INSERT INTO `tcg_classes` VALUES (20, '汉语言文学17', 50, 25, 3);
INSERT INTO `tcg_classes` VALUES (21, '汉语言文学18', 50, 25, 3);
INSERT INTO `tcg_classes` VALUES (22, '汉语言文学19', 50, 25, 3);

-- ----------------------------
-- Table structure for tcg_classes_course_teacher
-- ----------------------------
DROP TABLE IF EXISTS `tcg_classes_course_teacher`;
CREATE TABLE `tcg_classes_course_teacher`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '序号',
  `classes_id` bigint NOT NULL COMMENT '班级id',
  `course_id` bigint NOT NULL COMMENT '课程id',
  `teacher_id` bigint NOT NULL COMMENT '教师id',
  `week_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '星期 课 ABCDE 01',
  `classroom_id` bigint NULL DEFAULT NULL COMMENT '随机分配教室id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '班级课程教师关联表（分配课程，为排课做准备）' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tcg_classes_course_teacher
-- ----------------------------
INSERT INTO `tcg_classes_course_teacher` VALUES (1, 1, 1, 1, 'A5:09\\10', 13);
INSERT INTO `tcg_classes_course_teacher` VALUES (2, 1, 2, 3, 'A3:05\\06', 17);
INSERT INTO `tcg_classes_course_teacher` VALUES (3, 1, 3, 4, 'E1:01\\02', 10);
INSERT INTO `tcg_classes_course_teacher` VALUES (4, 1, 4, 1, 'D4:07\\08', 7);
INSERT INTO `tcg_classes_course_teacher` VALUES (5, 1, 5, 1, 'A2:03\\04', 10);
INSERT INTO `tcg_classes_course_teacher` VALUES (6, 1, 6, 1, 'B5:09\\10', 6);
INSERT INTO `tcg_classes_course_teacher` VALUES (7, 1, 13, 3, 'D2:03\\04', 17);
INSERT INTO `tcg_classes_course_teacher` VALUES (8, 1, 14, 3, 'C4:07\\08', 17);
INSERT INTO `tcg_classes_course_teacher` VALUES (9, 1, 15, 3, 'A1:01\\02', 20);
INSERT INTO `tcg_classes_course_teacher` VALUES (10, 1, 7, 1, 'A2:03\\04', 8);
INSERT INTO `tcg_classes_course_teacher` VALUES (11, 1, 8, 1, 'D2:03\\04', 8);
INSERT INTO `tcg_classes_course_teacher` VALUES (12, 1, 16, 3, 'D3:05\\06', 18);
INSERT INTO `tcg_classes_course_teacher` VALUES (13, 1, 17, 3, 'E1:01\\02', 18);

-- ----------------------------
-- Table structure for tcg_classroom
-- ----------------------------
DROP TABLE IF EXISTS `tcg_classroom`;
CREATE TABLE `tcg_classroom`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '序号',
  `classroom_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名字',
  `classroom_size` int NOT NULL DEFAULT 50 COMMENT '教室大小',
  `classroom_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '教室类型（普通教室0，实验室1，A会议室）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '教室' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tcg_classroom
-- ----------------------------
INSERT INTO `tcg_classroom` VALUES (1, '会议室01', 20, 'A');
INSERT INTO `tcg_classroom` VALUES (2, '普通教室1', 50, '0');
INSERT INTO `tcg_classroom` VALUES (3, '实验室1', 50, '1');
INSERT INTO `tcg_classroom` VALUES (4, '普通教室2', 20, '0');
INSERT INTO `tcg_classroom` VALUES (5, '实验室2', 20, '1');
INSERT INTO `tcg_classroom` VALUES (6, '普通教室3', 50, '0');
INSERT INTO `tcg_classroom` VALUES (7, '普通教室4', 50, '0');
INSERT INTO `tcg_classroom` VALUES (8, '普通教室5', 50, '0');
INSERT INTO `tcg_classroom` VALUES (9, '普通教室6', 50, '0');
INSERT INTO `tcg_classroom` VALUES (10, '普通教室7', 50, '0');
INSERT INTO `tcg_classroom` VALUES (11, '普通教室8', 50, '0');
INSERT INTO `tcg_classroom` VALUES (12, '普通教室9', 50, '0');
INSERT INTO `tcg_classroom` VALUES (13, '普通教室10', 50, '0');
INSERT INTO `tcg_classroom` VALUES (14, '实验室3', 50, '1');
INSERT INTO `tcg_classroom` VALUES (15, '实验室4', 50, '1');
INSERT INTO `tcg_classroom` VALUES (16, '实验室5', 50, '1');
INSERT INTO `tcg_classroom` VALUES (17, '实验室6', 50, '1');
INSERT INTO `tcg_classroom` VALUES (18, '实验室7', 50, '1');
INSERT INTO `tcg_classroom` VALUES (19, '实验室8', 50, '1');
INSERT INTO `tcg_classroom` VALUES (20, '实验室9', 50, '1');
INSERT INTO `tcg_classroom` VALUES (21, '实验室10', 50, '1');

-- ----------------------------
-- Table structure for tcg_course
-- ----------------------------
DROP TABLE IF EXISTS `tcg_course`;
CREATE TABLE `tcg_course`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '序号',
  `course_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名字',
  `course_book` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '20' COMMENT '教材',
  `course_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '课程类型（0理论课，1实验课）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '课程' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tcg_course
-- ----------------------------
INSERT INTO `tcg_course` VALUES (1, '物联网理论课01', '物联网理论课01', '0');
INSERT INTO `tcg_course` VALUES (2, '物联网实验课01', '物联网实验课01', '1');
INSERT INTO `tcg_course` VALUES (3, '物联网理论课1', '物联网理论课1', '0');
INSERT INTO `tcg_course` VALUES (4, '物联网理论课2', '物联网理论课2', '0');
INSERT INTO `tcg_course` VALUES (5, '物联网理论课3', '物联网理论课3', '0');
INSERT INTO `tcg_course` VALUES (6, '物联网理论课4', '物联网理论课4', '0');
INSERT INTO `tcg_course` VALUES (7, '物联网理论课5', '物联网理论课5', '0');
INSERT INTO `tcg_course` VALUES (8, '物联网理论课6', '物联网理论课6', '0');
INSERT INTO `tcg_course` VALUES (9, '物联网理论课7', '物联网理论课7', '0');
INSERT INTO `tcg_course` VALUES (10, '物联网理论课8', '物联网理论课8', '0');
INSERT INTO `tcg_course` VALUES (11, '物联网理论课9', '物联网理论课9', '0');
INSERT INTO `tcg_course` VALUES (12, '物联网理论课10', '物联网理论课10', '0');
INSERT INTO `tcg_course` VALUES (13, '物联网实验课1', '物联网实验课1', '1');
INSERT INTO `tcg_course` VALUES (14, '物联网实验课2', '物联网实验课2', '1');
INSERT INTO `tcg_course` VALUES (15, '物联网实验课3', '物联网实验课3', '1');
INSERT INTO `tcg_course` VALUES (16, '物联网实验课4', '物联网实验课4', '1');
INSERT INTO `tcg_course` VALUES (17, '物联网实验课5', '物联网实验课5', '1');
INSERT INTO `tcg_course` VALUES (18, '物联网实验课6', '物联网实验课6', '1');
INSERT INTO `tcg_course` VALUES (19, '物联网实验课7', '物联网实验课7', '1');
INSERT INTO `tcg_course` VALUES (20, '物联网实验课8', '物联网实验课8', '1');
INSERT INTO `tcg_course` VALUES (21, '物联网实验课9', '物联网实验课9', '1');
INSERT INTO `tcg_course` VALUES (22, '物联网实验课10', '物联网实验课10', '1');

-- ----------------------------
-- Table structure for tcg_professional
-- ----------------------------
DROP TABLE IF EXISTS `tcg_professional`;
CREATE TABLE `tcg_professional`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '序号',
  `professional_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名字',
  `professional_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '编码',
  `professional_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '专业类型（理学、工学、文学、法学、农学、医学、哲学、管理学、教育学、历史学、经济学、艺术学）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '专业' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tcg_professional
-- ----------------------------
INSERT INTO `tcg_professional` VALUES (1, '数学与应用数学', '1a1', 'a');
INSERT INTO `tcg_professional` VALUES (2, '物联网工程', '1b1', 'b');
INSERT INTO `tcg_professional` VALUES (3, '汉语言文学', '1c1', 'c');
INSERT INTO `tcg_professional` VALUES (4, '知识产权', '1d1', 'd');
INSERT INTO `tcg_professional` VALUES (5, '园林', '1e1', 'e');

-- ----------------------------
-- Table structure for tcg_teacher
-- ----------------------------
DROP TABLE IF EXISTS `tcg_teacher`;
CREATE TABLE `tcg_teacher`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '序号',
  `teacher_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '姓名',
  `teacher_telephone` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '电话',
  `teacher_week_max` int NOT NULL DEFAULT 20 COMMENT '每周课程上限',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '老师' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tcg_teacher
-- ----------------------------
INSERT INTO `tcg_teacher` VALUES (1, '物联网理论老师01', '18973202222', 25);
INSERT INTO `tcg_teacher` VALUES (2, '物联网辅导员', '18973202222', 0);
INSERT INTO `tcg_teacher` VALUES (3, '物联网实验老师01', '13677772222', 25);
INSERT INTO `tcg_teacher` VALUES (4, '老师1', '13200320000', 5);
INSERT INTO `tcg_teacher` VALUES (5, '老师2', '13200320000', 5);
INSERT INTO `tcg_teacher` VALUES (6, '老师3', '13200320000', 5);
INSERT INTO `tcg_teacher` VALUES (7, '老师4', '13200320000', 5);
INSERT INTO `tcg_teacher` VALUES (8, '老师5', '13200320000', 5);
INSERT INTO `tcg_teacher` VALUES (9, '老师6', '13200320000', 5);
INSERT INTO `tcg_teacher` VALUES (10, '老师7', '13200320000', 5);
INSERT INTO `tcg_teacher` VALUES (11, '老师8', '13200320000', 5);
INSERT INTO `tcg_teacher` VALUES (12, '老师9', '13200320000', 5);
INSERT INTO `tcg_teacher` VALUES (13, '老师10', '13200320000', 5);

SET FOREIGN_KEY_CHECKS = 1;
