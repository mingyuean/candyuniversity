/*
 Navicat MySQL Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80029
 Source Host           : localhost:3306
 Source Schema         : univ_nomenclature

 Target Server Type    : MySQL
 Target Server Version : 80029
 File Encoding         : 65001

 Date: 07/06/2022 21:40:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tee_classification
-- ----------------------------
DROP TABLE IF EXISTS `tee_classification`;
CREATE TABLE `tee_classification`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '序号',
  `classification_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名字',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '分类管理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tee_classification
-- ----------------------------
INSERT INTO `tee_classification` VALUES (1, '计算机');
INSERT INTO `tee_classification` VALUES (2, '分类');
INSERT INTO `tee_classification` VALUES (3, '分类');
INSERT INTO `tee_classification` VALUES (4, '分类');
INSERT INTO `tee_classification` VALUES (5, '分类');
INSERT INTO `tee_classification` VALUES (6, '分类');
INSERT INTO `tee_classification` VALUES (7, '分类');
INSERT INTO `tee_classification` VALUES (8, '分类');
INSERT INTO `tee_classification` VALUES (9, '分类');
INSERT INTO `tee_classification` VALUES (10, '分类');
INSERT INTO `tee_classification` VALUES (11, '分类');
INSERT INTO `tee_classification` VALUES (12, '分类');
INSERT INTO `tee_classification` VALUES (13, '分类');
INSERT INTO `tee_classification` VALUES (14, '分类');
INSERT INTO `tee_classification` VALUES (15, '分类');
INSERT INTO `tee_classification` VALUES (16, '分类');
INSERT INTO `tee_classification` VALUES (17, '分类');
INSERT INTO `tee_classification` VALUES (18, '分类');
INSERT INTO `tee_classification` VALUES (19, '分类');
INSERT INTO `tee_classification` VALUES (20, '分类');

-- ----------------------------
-- Table structure for tee_simplewrite
-- ----------------------------
DROP TABLE IF EXISTS `tee_simplewrite`;
CREATE TABLE `tee_simplewrite`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '序号',
  `simplewrite_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '缩写名',
  `simplewrite_describe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '缩写管理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tee_simplewrite
-- ----------------------------
INSERT INTO `tee_simplewrite` VALUES (1, 'cn', '中文');

-- ----------------------------
-- Table structure for tee_vocabulary
-- ----------------------------
DROP TABLE IF EXISTS `tee_vocabulary`;
CREATE TABLE `tee_vocabulary`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '序号',
  `vocabulary_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名字',
  `vocabulary_describe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `classification_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '分类id',
  `classification_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '分类名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '词汇管理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tee_vocabulary
-- ----------------------------
INSERT INTO `tee_vocabulary` VALUES (1, 'java', '一种编程语言', '1', '计算机');
INSERT INTO `tee_vocabulary` VALUES (2, 'AOP', '面向切面编程', '1', '计算机');
INSERT INTO `tee_vocabulary` VALUES (3, '通知（Advice)', '计算机中，面向切面编程，除目标方法之外的操作', '1', '计算机');
INSERT INTO `tee_vocabulary` VALUES (4, '切入点（Pointcut）', '计算机，面向切面编程', '1', '计算机');
INSERT INTO `tee_vocabulary` VALUES (5, '切面', '切面（Aspect）=通知（Advice）+切入点（Pointcut）', '1', '计算机');

SET FOREIGN_KEY_CHECKS = 1;
