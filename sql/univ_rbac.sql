/*
 Navicat MySQL Data Transfer

 Source Server         : test
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : localhost:3306
 Source Schema         : univ_rbac

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 13/05/2022 22:56:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for rbac_menu
-- ----------------------------
DROP TABLE IF EXISTS `rbac_menu`;
CREATE TABLE `rbac_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `menu_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '权限名',
  `menu_mark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '标识',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '路由地址',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '图标',
  `grade` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '等级',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '组件路径',
  `status` int(2) NOT NULL DEFAULT 0 COMMENT '状态（0正常 1停用）',
  `visible` int(2) NOT NULL DEFAULT 0 COMMENT '状态（0显示 1隐藏）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '权限表（菜单表）' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rbac_menu
-- ----------------------------
INSERT INTO `rbac_menu` VALUES (1, '用户管理', 'sys:user:manage', NULL, NULL, NULL, NULL, 0, 0);
INSERT INTO `rbac_menu` VALUES (2, '用户列表', 'sys:user:list', NULL, NULL, NULL, NULL, 0, 0);
INSERT INTO `rbac_menu` VALUES (3, '添加用户', 'sys:user:add', NULL, NULL, NULL, NULL, 0, 0);
INSERT INTO `rbac_menu` VALUES (4, '编辑用户', 'sys:user:edit', NULL, NULL, NULL, NULL, 0, 0);
INSERT INTO `rbac_menu` VALUES (5, '删除用户', 'sys:user:delete', NULL, NULL, NULL, NULL, 0, 0);
INSERT INTO `rbac_menu` VALUES (6, '教室列表', 'sys:classroom:list', NULL, NULL, NULL, NULL, 0, 0);

-- ----------------------------
-- Table structure for rbac_role
-- ----------------------------
DROP TABLE IF EXISTS `rbac_role`;
CREATE TABLE `rbac_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `role_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '角色名',
  `role_mark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '标识',
  `status` int(2) NOT NULL DEFAULT 0 COMMENT '状态（0正常 1停用）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '角色' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rbac_role
-- ----------------------------
INSERT INTO `rbac_role` VALUES (1, '管理员', 'r_admin', 0);
INSERT INTO `rbac_role` VALUES (2, '用户', 'r_user', 0);

-- ----------------------------
-- Table structure for rbac_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `rbac_role_menu`;
CREATE TABLE `rbac_role_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `role_id` bigint(20) NOT NULL COMMENT '角色id',
  `menu_id` bigint(20) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `role_menu_ibfk_1`(`menu_id`) USING BTREE,
  INDEX `role_menu_ibfk_2`(`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '角色权限中间表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rbac_role_menu
-- ----------------------------
INSERT INTO `rbac_role_menu` VALUES (1, 1, 1);
INSERT INTO `rbac_role_menu` VALUES (2, 1, 2);
INSERT INTO `rbac_role_menu` VALUES (3, 1, 3);
INSERT INTO `rbac_role_menu` VALUES (4, 1, 4);
INSERT INTO `rbac_role_menu` VALUES (5, 1, 5);
INSERT INTO `rbac_role_menu` VALUES (6, 2, 6);

-- ----------------------------
-- Table structure for rbac_user
-- ----------------------------
DROP TABLE IF EXISTS `rbac_user`;
CREATE TABLE `rbac_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '姓名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rbac_user
-- ----------------------------
INSERT INTO `rbac_user` VALUES (1, '123456', '$2a$10$CGQ1ESsaQ0SjXqsJrX.JWetvjhNP/SxQja8ud2vCoIoJ.y2HV/7Jq');
INSERT INTO `rbac_user` VALUES (2, 'myxxy1', '$2a$10$CGQ1ESsaQ0SjXqsJrX.JWetvjhNP/SxQja8ud2vCoIoJ.y2HV/7Jq');
INSERT INTO `rbac_user` VALUES (3, 'myxxy2', '$2a$10$CGQ1ESsaQ0SjXqsJrX.JWetvjhNP/SxQja8ud2vCoIoJ.y2HV/7Jq');
INSERT INTO `rbac_user` VALUES (4, 'myxxy3', '$2a$10$CGQ1ESsaQ0SjXqsJrX.JWetvjhNP/SxQja8ud2vCoIoJ.y2HV/7Jq');
INSERT INTO `rbac_user` VALUES (5, 'myxxy4', '$2a$10$CGQ1ESsaQ0SjXqsJrX.JWetvjhNP/SxQja8ud2vCoIoJ.y2HV/7Jq');
INSERT INTO `rbac_user` VALUES (6, 'myxxy5', '$2a$10$CGQ1ESsaQ0SjXqsJrX.JWetvjhNP/SxQja8ud2vCoIoJ.y2HV/7Jq');

-- ----------------------------
-- Table structure for rbac_user_role
-- ----------------------------
DROP TABLE IF EXISTS `rbac_user_role`;
CREATE TABLE `rbac_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `role_id` bigint(20) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_role_ibfk_1`(`user_id`) USING BTREE,
  INDEX `user_role_ibfk_2`(`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户角色中间表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rbac_user_role
-- ----------------------------
INSERT INTO `rbac_user_role` VALUES (1, 1, 1);
INSERT INTO `rbac_user_role` VALUES (2, 2, 2);
INSERT INTO `rbac_user_role` VALUES (3, 3, 2);
INSERT INTO `rbac_user_role` VALUES (4, 1, 2);
INSERT INTO `rbac_user_role` VALUES (5, 4, 2);
INSERT INTO `rbac_user_role` VALUES (6, 5, 2);
INSERT INTO `rbac_user_role` VALUES (7, 6, 2);

SET FOREIGN_KEY_CHECKS = 1;
