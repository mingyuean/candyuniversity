/*
 Navicat MySQL Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80029
 Source Host           : localhost:3306
 Source Schema         : univ_teaching

 Target Server Type    : MySQL
 Target Server Version : 80029
 File Encoding         : 65001

 Date: 02/06/2022 16:44:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tcg_classes
-- ----------------------------
DROP TABLE IF EXISTS `tcg_classes`;
CREATE TABLE `tcg_classes`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '序号',
  `classes_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名字',
  `classes_number` int NOT NULL DEFAULT 50 COMMENT '人数',
  `classes_week_max` int NOT NULL DEFAULT 8 COMMENT '每周课程上限',
  `professional_id` bigint NULL DEFAULT NULL COMMENT '专业id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '班级' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tcg_classes
-- ----------------------------

-- ----------------------------
-- Table structure for tcg_classes_course_teacher
-- ----------------------------
DROP TABLE IF EXISTS `tcg_classes_course_teacher`;
CREATE TABLE `tcg_classes_course_teacher`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '序号',
  `classes_id` bigint NOT NULL COMMENT '班级id',
  `course_id` bigint NOT NULL COMMENT '课程id',
  `teacher_id` bigint NOT NULL COMMENT '教师id',
  `week_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '星期 课 ABCDE 01',
  `classroom_id` bigint NULL DEFAULT NULL COMMENT '随机分配教室id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '班级课程教师关联表（分配课程，为排课做准备）' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tcg_classes_course_teacher
-- ----------------------------

-- ----------------------------
-- Table structure for tcg_classroom
-- ----------------------------
DROP TABLE IF EXISTS `tcg_classroom`;
CREATE TABLE `tcg_classroom`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '序号',
  `classroom_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名字',
  `classroom_size` int NULL DEFAULT 50 COMMENT '教室大小',
  `classroom_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0' COMMENT '教室类型（普通教室0，实验室1，A会议室）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '教室' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tcg_classroom
-- ----------------------------

-- ----------------------------
-- Table structure for tcg_course
-- ----------------------------
DROP TABLE IF EXISTS `tcg_course`;
CREATE TABLE `tcg_course`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '序号',
  `course_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名字',
  `course_book` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '20' COMMENT '教材',
  `course_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0' COMMENT '课程类型（0理论课，1实验课）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '课程' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tcg_course
-- ----------------------------

-- ----------------------------
-- Table structure for tcg_professional
-- ----------------------------
DROP TABLE IF EXISTS `tcg_professional`;
CREATE TABLE `tcg_professional`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '序号',
  `professional_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名字',
  `professional_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '编码',
  `professional_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '专业类型（理学、工学、文学、法学、农学、医学、哲学、管理学、教育学、历史学、经济学、艺术学）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '专业' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tcg_professional
-- ----------------------------

-- ----------------------------
-- Table structure for tcg_teacher
-- ----------------------------
DROP TABLE IF EXISTS `tcg_teacher`;
CREATE TABLE `tcg_teacher`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '序号',
  `teacher_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '姓名',
  `teacher_telephone` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '电话',
  `teacher_week_max` int NULL DEFAULT 20 COMMENT '每周课程上限',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '老师' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tcg_teacher
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
