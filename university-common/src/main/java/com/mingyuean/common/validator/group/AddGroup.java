/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package com.mingyuean.common.validator.group;

/**
 * 新增数据 Group
 * 校验分组
 */
public interface AddGroup {
}
