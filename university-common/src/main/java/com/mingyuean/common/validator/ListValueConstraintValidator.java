package com.mingyuean.common.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.Set;

/**
 * 自定义校验器
 * @author MingYueAn
 * @Description: 自定义校验器
 * @Date: 2022/5/14 16:48
 * @Version: 1.0
 */
public class ListValueConstraintValidator implements ConstraintValidator<ListValue, Integer> {

    private final Set<Integer> set = new HashSet<>();

    /**
     * 初始化方法
     *
     * @param constraintAnnotation 自定义注解的详细信息
     */
    @Override
    public void initialize(ListValue constraintAnnotation) {
        final int[] values = constraintAnnotation.values();
        for (int v : values) {
            set.add(v);
        }
    }

    /**
     * 判断是否校验成功
     *
     * @param integer                    需要校验的字段
     * @param constraintValidatorContext 上下文环境信息
     * @return true false
     */
    @Override
    public boolean isValid(Integer integer, ConstraintValidatorContext constraintValidatorContext) {
        return set.contains(integer);
    }
}
