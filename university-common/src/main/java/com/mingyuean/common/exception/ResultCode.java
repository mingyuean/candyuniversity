package com.mingyuean.common.exception;

/**
 * 响应码枚举
 *
 * @Author: MingYueAn
 * @Description: 返回码code
 * @Date: 2022/4/24 23:02
 * @Version: 1.0
 */
public enum ResultCode {
//前两位为业务场景
//10：通用  11：教师  12：权限  13：论坛
//后三位为错误码

    //10系列通用错误
    ERROR(10000, "未知错误"),
    VALID_EXCEPTION(10400, "参数校验异常"),
    SAVE_EXCEPTION(10201,"保存异常"),
    UPDATE_EXCEPTION(10202,"修改异常"),
    DELETE_EXCEPTION(10203,"删除异常"),
    CURRICULUM_EXCEPTION(10100, "排课异常");

    private int code;
    private String message;

    ResultCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
