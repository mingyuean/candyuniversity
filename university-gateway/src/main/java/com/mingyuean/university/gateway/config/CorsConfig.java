package com.mingyuean.university.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

/**
 * Cors跨域请求
 *
 * @Author: MingYueAn
 * @Description: Cors跨域请求
 * @Date: 2022/5/9
 * @Version: 1.0
 */
@Configuration
public class CorsConfig {

    @Bean
    public CorsWebFilter corsWebFilter() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration corsConfiguration = new CorsConfiguration();
        //设置允许跨域请求的域名
        corsConfiguration.addAllowedOriginPattern("*");
        //设置允许的header属性
        corsConfiguration.addAllowedHeader("*");
        //设置允许请求的方式
        corsConfiguration.addAllowedMethod("*");
        //设置允许cookis
        corsConfiguration.setAllowCredentials(true);

        source.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsWebFilter(source);
    }
}