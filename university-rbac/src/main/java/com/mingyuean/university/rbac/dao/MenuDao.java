package com.mingyuean.university.rbac.dao;

import com.mingyuean.university.rbac.entity.MenuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 权限表（菜单表）
 * 
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 21:54:27
 */
@Mapper
public interface MenuDao extends BaseMapper<MenuEntity> {
	
}
