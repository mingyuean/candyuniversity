package com.mingyuean.university.rbac.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 权限表（菜单表）
 * 
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 21:54:27
 */
@Data
@TableName("rbac_menu")
public class MenuEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 序号
	 */
	@TableId
	private Long id;
	/**
	 * 权限名
	 */
	private String menuName;
	/**
	 * 标识
	 */
	private String menuMark;
	/**
	 * 路由地址
	 */
	private String url;
	/**
	 * 图标
	 */
	private String icon;
	/**
	 * 等级
	 */
	private String grade;
	/**
	 * 组件路径
	 */
	private String component;
	/**
	 * 状态（0正常 1停用）
	 */
	private Integer status;
	/**
	 * 状态（0显示 1隐藏）
	 */
	private Integer visible;

}
