package com.mingyuean.university.rbac.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mingyuean.university.rbac.entity.MenuEntity;
import com.mingyuean.university.rbac.service.MenuService;
import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.common.utils.R;



/**
 * 权限表（菜单表）
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 21:54:27
 */
@RestController
@RequestMapping("rbac/menu")
public class MenuController {
    @Autowired
    private MenuService menuService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("rbac:menu:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = menuService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("rbac:menu:info")
    public R info(@PathVariable("id") Long id){
		MenuEntity menu = menuService.getById(id);

        return R.ok().put("menu", menu);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("rbac:menu:save")
    public R save(@RequestBody MenuEntity menu){
		menuService.save(menu);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("rbac:menu:update")
    public R update(@RequestBody MenuEntity menu){
		menuService.updateById(menu);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("rbac:menu:delete")
    public R delete(@RequestBody Long[] ids){
		menuService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
