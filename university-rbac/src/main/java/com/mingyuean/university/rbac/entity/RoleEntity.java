package com.mingyuean.university.rbac.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 角色
 * 
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 21:54:27
 */
@Data
@TableName("rbac_role")
public class RoleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 序号
	 */
	@TableId
	private Long id;
	/**
	 * 角色名
	 */
	private String roleName;
	/**
	 * 标识
	 */
	private String roleMark;
	/**
	 * 状态（0正常 1停用）
	 */
	private Integer status;

}
