package com.mingyuean.university.rbac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.university.rbac.entity.MenuEntity;

import java.util.Map;

/**
 * 权限表（菜单表）
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 21:54:27
 */
public interface MenuService extends IService<MenuEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

