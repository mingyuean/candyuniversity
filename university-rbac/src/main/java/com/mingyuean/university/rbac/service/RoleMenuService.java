package com.mingyuean.university.rbac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mingyuean.common.utils.PageUtils;
import com.mingyuean.university.rbac.entity.RoleMenuEntity;

import java.util.Map;

/**
 * 角色权限中间表
 *
 * @author MingYueAn
 * @email MingYueAn@email.com
 * @date 2022-05-08 21:54:27
 */
public interface RoleMenuService extends IService<RoleMenuEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

